//
//  DownloadManagerViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import AVKit
import AVFoundation

class DownloadsViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.downloads

	@IBOutlet weak private var collectionViewContainer: NSView!
	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var btnDownloadAll: NSButton!
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!
	@IBOutlet weak private var errorView: NSView!
	@IBOutlet weak private var errorLabel: NSTextField!
	@IBOutlet weak private var btnReload: NSButton!
	@IBOutlet weak private var apiStatusView: NSView!
	@IBOutlet weak private var btnDownloadsManager: NSButton!

	var repo: CoursesRepository!
	let loginTitle = "Login"
	let reloadTitle = "Reload"
	var courses = [Course]()

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: CourseDownloadCollectionViewItem.self)

		//update ui
		customizeUI()


		//notification
//		addNotificationObservers()

	}

	override func viewWillAppear() {
		super.viewWillAppear()

		fetchCourses()
		btnDownloadAll.isEnabled = true
	}

	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "Downloads"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		collectionViewContainer.showTestBorder(color: R.color.separator)

		btnDownloadAll.image = R.image.downloads.tintedImage(tint: R.color.secondary)

		apiStatusView.wantsLayer = true
		apiStatusView.layer?.backgroundColor = NSColor.white.cgColor

		btnDownloadsManager.font = R.font.robotoBold(size: 12)
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewFlowLayout()
		flowLayout.scrollDirection = .vertical
		collectionView.collectionViewLayout = flowLayout
	}

	private func showProgress() {
		hideError()
		progressIndicator.startAnimation(nil)
		apiStatusView.isHidden = false
	}

	private func hideProgress() {
		progressIndicator.stopAnimation(nil)
		apiStatusView.isHidden = true
	}

	private func showError(error: String) {
		errorLabel.stringValue = error
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func showEmptyView() {
		errorLabel.stringValue = "Courses are empty"
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func hideError() {
		errorView.isHidden = true
		apiStatusView.isHidden = true
	}


	//MARK: Actions
	@IBAction private func actReload(_ sender: NSButton) {
		if sender.title == loginTitle {
			NSAlert.showLogoutConfirmationAlert { (isConfirmed) in
				guard isConfirmed else { return }
				LoginSession.current.logout()
			}
		}else {
			fetchCourses()
		}
	}

	@IBAction private func actShowDownloadManager(_ sender: NSButton) {
		showDownloadsManagerScene()
	}

	@IBAction private func actDownloadAllCourses(_ sender: NSButton) {
		sender.isEnabled = false
		for i in 0..<courses.count {
			courses[i].isEnableDownload = false
		}
		collectionView.reloadData()
		DownloadManager.shared.download(coursesList: courses)
		/*
		let alert = NSAlert.newAlert(message: "Download Started", info: "All Courses are added to download manager", style: .informational)
		alert.runModal()
		*/
	}

	//MARK: Navigation
	private func showDownloadCourseFilesScene(courseIndex: Int) {
		let scene = DownloadCourseFilesViewController.instantiate()
		let course = courses[courseIndex]
		scene.course = course
		scene.repo = repo
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: course.title))
	}

	private func showDownloadsManagerScene() {
		let scene = DownloadsManagerViewController.instantiate()
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: "Downloads Manager"))
	}

	//MARK: Api
	private func fetchCourses() {
		showProgress()
		repo.fetchCourses { [weak self] (result) in
			switch result {
			case .success(let courses):
				guard let self = self else { return }
				self.courses = courses
				self.hideProgress()
				self.reloadData()
			case .failure(let error):
				guard let self = self else { return }
				if error.localizedDescription == R.apiError.sessionExpired {
					self.btnReload.title = self.loginTitle
				}else {
					self.btnReload.title = self.reloadTitle
				}
				self.showError(error: error.localizedDescription)
			}
		}
	}

	private func reloadData() {
		if courses.isEmpty {
			showEmptyView()
		}else {
			collectionView.reloadData()
		}
	}

	//MARK: Notification
	/*
	func addNotificationObservers()  {
		NotificationCenter.default.addObserver(self, selector: #selector(fileDownloaded(_:)), name: NSNotification.Name.init(.fileDownloadCompleted), object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}

	@objc private func fileDownloaded(_ notification: NSNotification) {
		guard let newFile = notification.object as? CourseFile,
			let courseIndex = newFile.courseIndex,
			courseIndex < courses.count,
			let fileIndex = newFile.fileIndex,
			fileIndex < courses[courseIndex].files.count ,
			courses[courseIndex].files[fileIndex].id == newFile.id else{
			return
		}

		//updated file
		if newFile.download_count <= 0 {
			courses[courseIndex].files.remove(at: fileIndex)
		}else {
			courses[courseIndex].files[fileIndex] = newFile
		}

		//remove course if files are empty
		if courses[courseIndex].files.isEmpty {
			courses.remove(at: courseIndex)
		}

		reloadData()
	}*/
}

//MARK: NSCollectionViewDataSource
extension DownloadsViewController: NSCollectionViewDataSource {

	func numberOfSections(in collectionView: NSCollectionView) -> Int {
		return 1
	}

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return courses.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
		let courseItem: CourseDownloadCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		let course = courses[indexPath.item]

		//configure item
		courseItem.configure(slNo: indexPath.item, course: course)
		courseItem.delegate = self

		return courseItem
	}
}

//MARK: NSCollectionViewDelegate
extension DownloadsViewController: NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
		return NSSize(width: collectionView.frame.size.width, height: 70)
	}

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {

		if let indexPath = indexPaths.first {
			showDownloadCourseFilesScene(courseIndex: indexPath.item)
		}
		collectionView.deselectItems(at: indexPaths)
	}
}

//MARK: CourseDownloadCollectionViewItemDelegate
extension DownloadsViewController: CourseDownloadCollectionViewItemDelegate {

	func courseDownloadCollectionViewItem(didTapDownload cell: CourseDownloadCollectionViewItem) {
		cell.btlDownload.isEnabled = false
		guard let indexPath = collectionView.indexPath(for: cell) else { return }
		guard indexPath.item < courses.count else { return }
		courses[indexPath.item].isEnableDownload = false
		let course = courses[indexPath.item]
		DownloadManager.shared.download(coursesList: [course])
		/*
		DownloadManager.shared.download(course: course)
		let alert = NSAlert.newAlert(message: "Download Started", info: "Missing file in \(course.title) are added to download manager", style: .informational)
		alert.runModal()
		*/
	}
}
