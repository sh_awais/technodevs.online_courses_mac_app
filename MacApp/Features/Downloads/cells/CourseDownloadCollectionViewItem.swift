//
//  CourseDownloadCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 10/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

protocol CourseDownloadCollectionViewItemDelegate: class {
	func courseDownloadCollectionViewItem(didTapDownload cell: CourseDownloadCollectionViewItem)
}
class CourseDownloadCollectionViewItem: NSCollectionViewItem, NibReusable {

	@IBOutlet weak private var icon: NSImageView!
	@IBOutlet weak private var iconCard: NSView!
	@IBOutlet weak private var slNo: NSTextField!
	@IBOutlet weak private var titleLbl: NSTextField!
	@IBOutlet weak var btlDownload: NSButton!

//	var downloadCallback: ((NSCollectionViewItem) -> Void)?
	weak var delegate: CourseDownloadCollectionViewItemDelegate?

	override func awakeFromNib() {
		super.awakeFromNib()
		btlDownload.image = R.image.downloads.tintedImage(tint: R.color.secondary)
	}

	func configure(slNo: Int, course: Course) {

		ImageHelper.shared.image(forCourse: course, size: 100) { [weak self] (image) in
			self?.icon.image = image
		}
		self.slNo.stringValue = String(format: "%02d.", slNo)
		self.titleLbl.stringValue = course.title

		btlDownload.font = R.font.robotoBold(size: 14)
		btlDownload.isEnabled = course.isEnableDownload
	}

	@IBAction private func actDownload(_ sender: NSButton) {
		delegate?.courseDownloadCollectionViewItem(didTapDownload: self)
//		sender.isEnabled = false
	}

}
