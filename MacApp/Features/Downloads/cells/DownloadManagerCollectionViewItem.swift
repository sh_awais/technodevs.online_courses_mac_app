//
//  DownloadManagerCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 17/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

protocol DownloadManagerCollectionViewItemDelegate: class {
	func downloadManagerCollectionViewItem(didCompleteFileForCell cell: DownloadManagerCollectionViewItem)
}

class DownloadManagerCollectionViewItem: NSCollectionViewItem, NibReusable {

	@IBOutlet weak private var cardView: NSView!
	
	@IBOutlet weak private var iconContainer: NSView!
	@IBOutlet weak private var icon: NSImageView!
	@IBOutlet weak private var fileName: NSTextField!
	@IBOutlet weak private var fileInfo: NSTextField!

	@IBOutlet weak private var btnPrimary: NSButton!
	@IBOutlet weak private var btnSecondary: NSButton!

	@IBOutlet weak private var progressContainer: NSView!
	@IBOutlet weak private var progressPercentage: NSTextField!
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!

	weak var delegate: DownloadManagerCollectionViewItemDelegate?

	var file: CourseFile?

	///these properties are used for speed calculation
	var oldSpeed: Int64 = 0
	var prevBytesForSpeed: Int64 = 0
	var timer: Timer?

	override func awakeFromNib() {
		super.awakeFromNib()
//		customizeUI()
	}

	private func customizeUI() {
		iconContainer.wantsLayer = true
		iconContainer.layer?.backgroundColor = R.color.lightBackground.cgColor

		btnSecondary.font = R.font.robotoBold(size: 14)
		btnPrimary.font = R.font.robotoBold(size: 14)
		btnSecondary.image = R.image.cross.tintedImage(tint: R.color.secondary)

		self.cardView.showTestBorder(color: R.color.separator)
	}

	private func dummyData() {
		icon.image = R.image.audioRounded
		fileName.stringValue = "File name.mp4"
		fileInfo.stringValue = "256 kbs/s - 1234"

		btnPrimary.image = R.image.pause.tintedImage(tint: R.color.secondary)

		progressIndicator.doubleValue = 50
		progressPercentage.stringValue = "50%"
	}

	func  configure(file: CourseFile) {
		self.file = file

		fileName.stringValue = file.file_name

		switch file.fileExtension {
		case .mp3:
			icon.image = R.image.audioRounded
		case .mp4:
			icon.image = R.image.videoRounded
		case .pdf, .epub:
			icon.image = R.image.ebookRounded
		case .unknown:
			icon.image = NSImage()
		}

		configureStatus(file: file)
	}

	func sizeInMb(bytes: Int64) -> String {
		let bcf = ByteCountFormatter()
		bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
		bcf.countStyle = .file
		return  bcf.string(fromByteCount: bytes)
	}

	func speedInkbs(speedBps: Int64) -> String {
		let bcf = ByteCountFormatter()
		bcf.allowedUnits = [.useKB] // optional: restricts the units to MB only
		bcf.countStyle = .file
		return  bcf.string(fromByteCount: speedBps)
	}
	private func formatFileInfo(total: Int64, completed: Int64) -> String {
		// format example "0 kb/s - 0mb of 12mb"
		let totalMb = sizeInMb(bytes: total)
		let completedMb = sizeInMb(bytes: completed)
		return "\(speedInkbs(speedBps: oldSpeed))/s  - \(completedMb) of \(totalMb)"
	}

	private func configureStatus(file: CourseFile) {
		let primaryBtnInfo: ButtonInfo
		let fileInfo: FileInfo

		btnPrimary.stringValue = "Alpha"
		switch file.downloadStatus {
		case .downloading(let progress):
			configureProgress(progress: progress)
			primaryBtnInfo = ButtonInfo(title: "  Pause",
										icon: R.image.pause.tintedImage(tint: R.color.secondary))

			let title = formatFileInfo(total: file.totalBytes, completed: file.completedBytes)
			fileInfo = FileInfo(title: title,
								color: R.color.secondaryTextDark)
			progressContainer.isHidden = false
		case .paused:
			primaryBtnInfo = ButtonInfo(title: "  Play",
										icon: R.image.play.tintedImage(tint: R.color.secondary))
			fileInfo = FileInfo(title: "Pending...", color: R.color.secondaryTextDark)
			progressContainer.isHidden = false
		case .failed:
			primaryBtnInfo = ButtonInfo(title: "  Retry",
										icon: R.image.retry.tintedImage(tint: R.color.secondary))
			fileInfo = FileInfo(title: "Error - please retry your downloading...", color: R.color.primary)
			progressContainer.isHidden = true

		default:
			primaryBtnInfo = ButtonInfo(title: "----", icon: NSImage())
			fileInfo = FileInfo(title: "----", color: R.color.secondaryTextDark)
		}

		configurePrimaryButton(btnInfo: primaryBtnInfo)
		btnPrimary.tag = file.downloadStatus.toInt().0
		configureFileInfo(info: fileInfo)
	}

	private func configurePrimaryButton(btnInfo: ButtonInfo) {
		btnPrimary.title = btnInfo.title
		btnPrimary.attributedTitle = btnPrimary.titleWithColor(color: R.color.primaryTextLight)
		btnPrimary.image = btnInfo.icon
	}

	private func configureFileInfo(info: FileInfo) {
		self.fileInfo.textColor = info.color
		self.fileInfo.stringValue = info.title
	}

	private func configureProgress(progress: Double) {
		print(progress)
		self.progressPercentage.stringValue = "\(Int(progress))%"
		self.progressIndicator.doubleValue = progress
	}

	//MARK: Actions
	@IBAction private func actPrimary(_ sender: NSButton) {
		guard let file = self.file else {
			return
		}

		let status = FileDownloadStatus.fromInt(status: sender.tag, progress: 0)
		switch status {
		case .downloading(_):
			DownloadManager.shared.pause(file: file)
		case .paused(_):
			DownloadManager.shared.play(file: file)
		case .failed, .notStarted,.downloaded:
			DownloadManager.shared.download(file: file)
		}
	}

	@IBAction private func actCancel(_ sender: NSButton) {
		if let file = file {
			DownloadManager.shared.cancel(file: file)
		}
	}

	//MARK: Notifications
	func addNotificationObservers()  {
		NotificationCenter.default.addObserver(self, selector: #selector(progressUpdated(_:)), name: NSNotification.Name.init(.fileProgressUpdated), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(fileDownloaded(_:)), name: NSNotification.Name.init(.fileDownloadCompleted), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(downloadingPaused(_:)), name: NSNotification.Name.init(.fileDownloadingPaused), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(downloadingPaused(_:)), name: NSNotification.Name.init(.fileDownloadingFailed), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(downloadingCanceled(_:)), name: NSNotification.Name.init(.fileDownloadingCanceled), object: nil)
	}

	@objc private func progressUpdated(_ notification: NSNotification) {
		fileStatusUpdatedNotification(notification)
	}

	@objc private func fileDownloaded(_ notification: NSNotification) {
		delegate?.downloadManagerCollectionViewItem(didCompleteFileForCell: self)
	}

	@objc private func downloadingCanceled(_ notification: NSNotification) {
		delegate?.downloadManagerCollectionViewItem(didCompleteFileForCell: self)
	}

	@objc private func downloadingFailed(_ notification: NSNotification) {
		fileStatusUpdatedNotification(notification)
	}

	@objc private func downloadingPaused(_ notification: NSNotification) {
		fileStatusUpdatedNotification(notification)
	}

	private func fileStatusUpdatedNotification(_ notification: NSNotification) {
		guard let newFile = notification.object as? CourseFile, let existingFileId = self.file?.id else {
			return
		}

		if newFile.id == existingFileId {
			self.file = newFile
			configureStatus(file: newFile)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		addNotificationObservers()
		customizeUI()
		startSpeedTimer()
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
		stopSpeedTimer()
	}

	//MARK: Timer
	func startSpeedTimer(){
		stopSpeedTimer()
		timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] (_) in
			guard let self = self else { return }
			let newSpeed = (self.file?.completedBytes ?? 0) - self.prevBytesForSpeed
			if newSpeed > 0 {
				self.oldSpeed = newSpeed
				self.prevBytesForSpeed = self.file?.completedBytes ?? 0
			}
		})
	}

	func stopSpeedTimer()  {
		timer?.invalidate()
		timer = nil
	}
}

//MARK: Structures
extension DownloadManagerCollectionViewItem {
	private struct ButtonInfo{
		let title: String
		let icon: NSImage?
	}

	private struct FileInfo{
		let title: String
		let color: NSColor
	}
}
