//
//  DownloadManagerViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import AVKit
import AVFoundation
import SDWebImage

class DownloadCourseFilesViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.downloads

	@IBOutlet weak private var courseImage: NSImageView!
	@IBOutlet weak private var collectionViewContainer: NSView!
	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var btnDownloadAll: NSButton!
	@IBOutlet weak private var btnDownloadsManager: NSButton!

	//Api state view
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!
	@IBOutlet weak private var errorView: NSView!
	@IBOutlet weak private var errorLabel: NSTextField!
	@IBOutlet weak private var btnReload: NSButton!
	@IBOutlet weak private var apiStatusView: NSView!

	let loginTitle = "Login"
	let reloadTitle = "Reload"
//	var courseId: Int = 0
	var course = Course()
	var repo: CoursesRepository!
//	var courseIndex: Int = 0

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: CourseFileCollectionViewItem.self)

		//update ui
		customizeUI()

		headerView.title.stringValue = course.title

		//set course image
		ImageHelper.shared.image(forCourse: course, size: 500) { [weak self] (image) in
			self?.courseImage.image = image
		}

	}

	override func viewWillAppear() {
		super.viewWillAppear()

		fetchCourseDetails(courseId: course.id)
		btnDownloadAll.isEnabled = true
	}
	
	private func reloadData() {
		if course.files.isEmpty {
			showEmptyView()
		}else {
			collectionView.reloadData()
		}
	}

	//MARK: UI
	private func customizeUI() {
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		collectionViewContainer.showTestBorder(color: R.color.separator)

		apiStatusView.wantsLayer = true
		apiStatusView.layer?.backgroundColor = NSColor.white.cgColor

		btnDownloadAll.image = R.image.downloads.tintedImage(tint: R.color.secondary)

		btnDownloadsManager.font = R.font.robotoBold(size: 12)
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewGridLayout()
		flowLayout.maximumNumberOfColumns = 1
		flowLayout.minimumItemSize = NSSize(width: collectionView.frame.size.height, height: 50)
		flowLayout.maximumItemSize = NSSize(width: 2000, height: 50)
		collectionView.collectionViewLayout = flowLayout
	}

	private func showProgress() {
		hideError()
		progressIndicator.startAnimation(nil)
		apiStatusView.isHidden = false
	}

	private func hideProgress() {
		progressIndicator.stopAnimation(nil)
		apiStatusView.isHidden = true
	}

	private func showError(error: String) {
		errorLabel.stringValue = error
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func showEmptyView() {
		errorLabel.stringValue = "Courses are empty"
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func hideError() {
		errorView.isHidden = true
		apiStatusView.isHidden = true
	}
	//MARK: Actions
	@IBAction private func actShowDownloadManager(_ sender: NSButton) {
		showDownloadsManagerScene()
	}
	
	@IBAction private func downloadAllFiles(_ sender: NSButton) {
		btnDownloadAll.isEnabled = false
		for i in 0..<course.files.count {
			course.files[i].downloadStatus = .downloading(0)
		}
		collectionView.reloadData()
		DownloadManager.shared.download(course: course)
	}

	@IBAction private func actReload(_ sender: NSButton) {
		if sender.title == loginTitle {
			NSAlert.showLogoutConfirmationAlert { (isConfirmed) in
				guard isConfirmed else { return }
				LoginSession.current.logout()
			}
		}else {
			fetchCourseDetails(courseId: course.id)
		}
	}

	private func showDownloadsManagerScene() {
		let scene = DownloadsManagerViewController.instantiate()
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: "Downloads Manager"))
	}
	
	//MARK: Api
	private func fetchCourseDetails(courseId: Int) {
		showProgress()
		repo.fechCourseDetails(courseId: courseId) { [weak self] (result) in
			switch result {
			case .success(let course):
				guard let self = self else { return }
				self.course = course
				self.hideProgress()
				self.reloadData()
			case .failure(let error):
				guard let self = self else { return }
				if error.localizedDescription == R.apiError.sessionExpired {
					self.btnReload.title = self.loginTitle
				}else {
					self.btnReload.title = self.reloadTitle
				}
				self.showError(error: error.localizedDescription)
			}
		}
	}
}

//MARK: NSCollectionViewDataSource
extension DownloadCourseFilesViewController: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return course.files.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
		
		let courseItem: CourseFileCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		//configure item
		var file = course.files[indexPath.item]
		file.courseTitle = course.title
		file.categoryTitle = course.category
		courseItem.configure(style: .download(indexPath.item, file))
		courseItem.delegate = self

		return courseItem
	}

	private func didTapDownload(cell: NSCollectionViewItem) {

		//get index from cell
		guard let index = self.collectionView.indexPath(for: cell) else { return }

		//update file status to downloading
		course.files[index.item].downloadStatus = .downloading(0)

		//update cell
		(cell as? CourseFileCollectionViewItem)?.configure(status: self.course.files[index.item].downloadStatus)

		//download file
		self.downloadFile(file: course.files[index.item], fileIndex: index.item)
	}

	private func downloadFile(file: CourseFile, fileIndex: Int) {

		var newFile = file
//		newFile.downloadUrl = downloadUrl
		newFile.categoryTitle = self.course.category
		newFile.courseTitle = self.course.title
		DownloadManager.shared.download(file: newFile)
		/*

		//fetch file download link
		CourseFileDownloadLink.fetchFile(token: LoginSession.current.token, fileId: file.id) { [weak self] (result) in

			guard let self = self else { return }
			switch result {
			case .success(let fileUrl):
				guard let downloadUrl = fileUrl.getUrl() else {
					return
				}

				//download file
				var newFile = file
				newFile.downloadUrl = downloadUrl
				newFile.categoryTitle = self.course.category
				newFile.courseTitle = self.course.title
				newFile.courseIndex = self.courseIndex
				newFile.fileIndex = fileIndex
				DownloadManager.shared.download(file: newFile)
			case .failure(let error):
				print(error.localizedDescription)
			}
		}*/
	}
}

//MARK: NSCollectionViewDelegate
extension DownloadCourseFilesViewController: NSCollectionViewDelegate {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
		collectionView.deselectItems(at: indexPaths)
	}
}

//MARK: CourseFileCollectionViewItemDelegate
extension DownloadCourseFilesViewController: CourseFileCollectionViewItemDelegate {

	func courseFileCollectionViewItem(fileDownloadedForCell cell: CourseFileCollectionViewItem,
									  newFile: CourseFile) {
		guard let indexPath = collectionView.indexPath(for: cell),
			indexPath.item < self.course.files.count,
			course.files[indexPath.item].id == newFile.id else {
				return
		}
		self.course.files[indexPath.item].download_count -= 1
		if self.course.files[indexPath.item].download_count <= 0 {
			self.course.files.remove(at: indexPath.item)
			collectionView.deleteItems(at: Set<IndexPath>(arrayLiteral: indexPath))
		}
	}

	func courseFileCollectionViewItem(downloadFileForCell cell: CourseFileCollectionViewItem) {
		didTapDownload(cell: cell)
	}
}

