//
//  DownloadManagerViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import AVKit
import AVFoundation
import SDWebImage

class DownloadsManagerViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.downloads

	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var emptyFilesView: NSView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var downloadLimit: NSPopUpButton!

	var files: [CourseFile] {
		return DownloadManager.shared.getList()
	}

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: DownloadManagerCollectionViewItem.self)

		//update ui
		customizeUI()
		
		//show message if courses are empty
		reloadData()

		DownloadManager.shared.delegate = self

		/// configure download limit
		downloadLimit.removeAllItems()
		for title in ["1","2","3","4","5"] {
			downloadLimit.addItem(withTitle: title)
		}
		selectDownloadLimit(limit: DownloadManager.shared.downloadLimit)
	}

	private func selectDownloadLimit(limit: Int) {
		for (index,title) in downloadLimit.itemTitles.enumerated() {
			if title == "\(limit)" {
				downloadLimit.selectItem(at: index)
			}
		}
	}
	private func reloadData() {
		if files.isEmpty {
			emptyFilesView.isHidden = false
			collectionView.isHidden = true
		}else {
			emptyFilesView.isHidden = true
			collectionView.isHidden = false
			collectionView.reloadData()
		}
	}

	//MARK: UI
	private func customizeUI() {
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)
		headerView.title.stringValue = "Downloads Manager"

		emptyFilesView.wantsLayer = true
		emptyFilesView.layer?.backgroundColor = NSColor.white.cgColor

	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewGridLayout()
		flowLayout.maximumNumberOfColumns = 1
		flowLayout.minimumItemSize = NSSize(width: collectionView.frame.size.height, height: 120)
		flowLayout.maximumItemSize = NSSize(width: 2000, height: 120)
		flowLayout.minimumInteritemSpacing = 0
		flowLayout.minimumLineSpacing = 16
		collectionView.collectionViewLayout = flowLayout
	}


	@IBAction func actDownloadLimit(_ sender: NSPopUpButton) {
		let title = sender.selectedItem?.title ?? ""
		DownloadManager.shared.downloadLimit = Int(title) ?? 4
	}
}

//MARK: NSCollectionViewDataSource
extension DownloadsManagerViewController: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return files.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
		
		let collectionViewItem: DownloadManagerCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		//configure item
		collectionViewItem.configure(file: files[indexPath.item])
		collectionViewItem.delegate = self

		return collectionViewItem
	}

}

//MARK: NSCollectionViewDelegate
extension DownloadsManagerViewController: NSCollectionViewDelegate {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
		collectionView.deselectItems(at: indexPaths)
	}
}

//MARK: DownloadManagerCollectionViewItemDelegate
extension DownloadsManagerViewController: DownloadManagerCollectionViewItemDelegate {

	func downloadManagerCollectionViewItem(didCompleteFileForCell cell: DownloadManagerCollectionViewItem) {
		reloadData()
	}
}


extension DownloadsManagerViewController: DownloadManagerDelegate {
	func downloadManager(historyUpdated: [CourseFile]) {
		self.reloadData()
	}
}
