//
//  WifiTransferViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import Swifter

class WifiTransferViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.wifiTransfer


	@IBOutlet weak private var btnStartServer: NSButton!
	@IBOutlet weak private var serverStateImage: NSImageView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var lblPort: NSTextField!
	@IBOutlet weak private var link: NSButton!

	private var wifiTransferLink: String {
		return "http://localhost:8080/wifiTransfer"
	}

	private var port: Int {
		do {
			return try AppDelegate.shared.server.port()
		}catch {
			return 0
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		customizeUI()	

	}
	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "Wifi Transfer"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		btnStartServer.font = R.font.robotoBold(size: 12)
		updateServerButton(forState: AppDelegate.shared.server.state)
		switch AppDelegate.shared.server.state {
		case .running:
			displayServerInfo(port: port, url: wifiTransferLink)
		default:
			hideServerInfo()
		}
	}

	private func updateServerButton(forState state: HttpServerIO.HttpServerIOState) {
		switch state {
		case .running:
			btnStartServer.title = "Stop Server"
			btnStartServer.attributedTitle = btnStartServer.titleWithColor(color: R.color.primaryTextLight)
			serverStateImage.image = R.image.serverHighlighted
		default:
			btnStartServer.title = "Start Server"
			btnStartServer.attributedTitle = btnStartServer.titleWithColor(color: R.color.primaryTextLight)
			serverStateImage.image = R.image.server
		}
	}

	private func displayServerInfo(port: Int, url: String) {
		self.lblPort.stringValue = "Port: \(port)"
		link.title = url
		self.link.attributedTitle = link.titleWithColor(color: R.color.primary)

		link.isHidden = false
		self.lblPort.isHidden = false
	}

	private func hideServerInfo() {
		link.isHidden = true
		lblPort.isHidden = true
	}

	//MARK: Actions
	@IBAction func actToggleServer(_ sender: NSButton) {
		if AppDelegate.shared.server.state == .running {
			AppDelegate.shared.server.stop()
			updateServerButton(forState: .stopped)
			hideServerInfo()
		}else {
			AppDelegate.shared.serverQueue.async { [weak self] in
				self?.startServer()
			}
			updateServerButton(forState: .running)
		}

	}

	@IBAction func actOpenLink(_ sender: NSButton) {
		if let url = URL(string: wifiTransferLink) {
			NSWorkspace.shared.open(url)
		}
	}

	private func startServer() {

		let semaphore = DispatchSemaphore(value: 0)
		do {

			let server = AppDelegate.shared.server

			let fileUpload = Bundle.main.url(forResource: "FileUpload", withExtension: "html")
			let rawHtml = try? String(contentsOf: fileUpload!, encoding: .utf8)
			let uploadPage = rawHtml ?? ""
			server["/wifiTransfer"] = { request in
				return HttpResponse.ok(.html(uploadPage))
			}

			server["/PostFile"] = { [weak self] request in  
				let data = request.parseMultiPartFormData()
				guard data.count >= 2 else {
					return HttpResponse.ok(.text("File not found"))
				}
				let fileData = Data(bytes: data[0].body)
				let fileName = String(bytes: data[1].body, encoding: .utf8) ?? ""

				//check extension
				let fileExtension = FileExtensions.fromString(fileName: fileName)
				guard fileExtension != .unknown else {
					return  HttpResponse.ok(.text("Files with extension \(fileExtension.rawValue) are not supported"))
				}

				//create directory
				let directory = TechnoDevDirectories.uploadsDirectory.mediaDirectory(fileExtension: fileExtension)
				guard let newDirectory = directory.createDirectory() else {
					return  HttpResponse.ok(.text("Failed to save file"))
				}
				self?.setUploadsImage(directory: TechnoDevDirectories.uploadsDirectory)

				let fileDirectory = newDirectory.appendingPathComponent(fileName)
				do {
					try fileData.write(to: fileDirectory)
					return HttpResponse.ok(.text("File Uploaded Successfully"))
				} catch {
					// failed to write file – bad permissions, bad filename, missing permissions, or more likely it can't be converted to the encoding
					return HttpResponse.ok(.text("File is corrupt"))
				}
			}

			try server.start(8080, forceIPv4: true)


			///update server info
			DispatchQueue.main.async { [weak self] in
				guard let self = self else { return }
//				let port = try? server.port()
				self.displayServerInfo(port: self.port, url: self.wifiTransferLink)
				self.actOpenLink(self.link)
			}

//			print("Server has started ( port = \(try server.port()) ). Try to connect now...")

			semaphore.wait()


		}catch {

			DispatchQueue.main.async {
				self.updateServerButton(forState: .stopped)
				self.hideServerInfo()
			}
			print("Server start error: \(error)")
			semaphore.signal()
		}
	}

	func setUploadsImage(directory: URL) {
		let imgPath = directory.appendingPathComponent("defaultx500.png")

		if !imgPath.isFileExists {
			let smallImg = directory.appendingPathComponent("defaultx100.png")
			_ = R.image.uploads.savePNG(to: imgPath)
			_ = R.image.uploads.savePNG(to: smallImg)
		}
	}

}

extension NSBitmapImageRep {
	var png: Data? {
		return representation(using: .png, properties: [:])
	}
}
extension Data {
	var bitmap: NSBitmapImageRep? {
		return NSBitmapImageRep(data: self)
	}
}
extension NSImage {
	var png: Data? {
		return tiffRepresentation?.bitmap?.png
	}
	func savePNG(to url: URL) -> Bool {
		do {
			try png?.write(to: url)
			return true
		} catch {
			print(error)
			return false
		}

	}
}
