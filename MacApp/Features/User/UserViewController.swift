//
//  UserViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class UserViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.user

	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var userInfoContainer: NSView!
	@IBOutlet weak private var higlightContainer: NSView!
	@IBOutlet weak private var firstName: NSTextField!
	@IBOutlet weak private var lastName: NSTextField!
	@IBOutlet weak private var company: NSTextField!

	@IBOutlet weak private var btnEdit: NSButton!
	@IBOutlet weak private var btnChangePass: NSButton!

	override func viewDidLoad() {
        super.viewDidLoad()
		//UI
		customizeUI()
    }

	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "User Details"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		userInfoContainer.showTestBorder(color: R.color.separator)

		higlightContainer.wantsLayer = true
		higlightContainer.layer?.backgroundColor = R.color.darkBackground.cgColor

		//user info
		firstName.stringValue = LoginSession.current.firstName
		lastName.stringValue = LoginSession.current.lastName
		company.stringValue = LoginSession.current.company

		btnEdit.font = R.font.robotoBold(size: 14)
		btnChangePass.font = R.font.robotoBold(size: 14)
	}

	
	//MARK: Actions
	@IBAction private func actChangePassword(_ sender: NSButton) {
		showChangePasswordScene()
	}

	@IBAction private func actEditProfile(_ sender: NSButton) {

	}

	//MARK: Navigation
	private func showChangePasswordScene() {
		let scene = ChangePasswordViewController.instantiate()
		scene.repo = UserRepository()
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: "Update Password"))
	}
}
