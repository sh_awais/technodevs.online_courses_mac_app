//
//  UserViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class ChangePasswordViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.user

	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var fieldOldPassword: NSTextField!
	@IBOutlet weak private var newPassword: NSTextField!
	@IBOutlet weak private var confirmPassword: NSTextField!

	@IBOutlet weak private var errorLabel: NSTextField!
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!

	@IBOutlet weak private var btnUpdatePass: NSButton!
	@IBOutlet weak private var btnCancel: NSButton!

	var repo: UserRepository!

	override func viewDidLoad() {
        super.viewDidLoad()
		//UI
		customizeUI()
    }

	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "Update Password"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		btnUpdatePass.font = R.font.robotoBold(size: 14)
		btnCancel.font = R.font.robotoBold(size: 14)
	}

	private func updateInputWidgets(isEnable: Bool) {
		fieldOldPassword.isEnabled = isEnable
		newPassword.isEnabled = isEnable
		confirmPassword.isEnabled = isEnable
		btnUpdatePass.isEnabled = isEnable
	}

	private func showProgress() {
		progressIndicator.startAnimation(nil)
		updateInputWidgets(isEnable: false)
	}

	private func hideProgress() {
		progressIndicator.stopAnimation(nil)
		updateInputWidgets(isEnable: true)
	}

	//MARK: Actions
	@IBAction private func actUpdatePassword(_ sender: NSButton) {
		guard isValid(currentPassword: fieldOldPassword.stringValue,
					  newPassword: newPassword.stringValue, confirmPassword: confirmPassword.stringValue) else {
						return
		}

		//input
		let userInput = UserInput(authToken: LoginSession.current.token.auth_token,
								  currentPassword: fieldOldPassword.stringValue)
		userInput.password = newPassword.stringValue

		//api call
		updatePassword(withCredentials: userInput)
	}

	@IBAction private func actCancel(_ sender: NSButton) {
		popScene()
	}

	//MARK: Data
	private func isValid(currentPassword: String, newPassword: String, confirmPassword: String) -> Bool {

		var message = ""

		//validate email
		if currentPassword.isEmpty {
			message = "Please enter your current password"
		}else if newPassword.isEmpty {
			message = "Password cannot be empty"
		}else if confirmPassword != newPassword {
			message = "Password and confirm password do no match"
		}

		errorLabel.stringValue = message

		if message.isEmpty {
			return true
		}else {
			return false
		}
	}

	//MARK: APi
	private func updatePassword(withCredentials credentials: UserInput) {
		showProgress()
		repo.updateUser(input: credentials) { [weak self] (result) in
			self?.hideProgress()
			switch result {
			case .success(let loginResponse):
				LoginSession.current.set(loginResponse: loginResponse)
				let alert = NSAlert.newAlert(message: "Password Updated", info: "", style: .informational)
				alert.addButton(withTitle: "OK")
				let response = alert.runModal()
				if response == NSApplication.ModalResponse.alertFirstButtonReturn {
//					self?.navigationController.popViewController(animated: true)
					self?.popScene()
				}
			case .failure(let error):
				self?.errorLabel.stringValue = error.localizedDescription
			}
		}
	}

	//MARK: Navigation
	private func popScene() {
		let nav = self.navigationController as? TechnoDevNavigationController
		nav?.removeLastBreadCrumItem()
	}
}
