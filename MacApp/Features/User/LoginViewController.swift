//
//  ViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import Moya

class LoginViewController: TechnoDevController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.user

	@IBOutlet weak private var fieldEmail: NSTextField!
	@IBOutlet weak private var fieldPassword: NSSecureTextField!
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!
	@IBOutlet weak private var errorLabel: NSTextField!
	@IBOutlet weak private var btnLogin: PrimaryButton!

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.expandToFulScreen()

		self.setSize(greaterThanEqualTo: R.dimension.screenSize)
	}
	
	override func viewDidAppear() {
		super.viewDidAppear()
		customizeUI()
	}

	//MARK: UI
	private func customizeUI() {
		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.white.cgColor
	}

	private func showProgress() {
		progressIndicator.startAnimation(nil)
		fieldEmail.isEnabled = false
		fieldPassword.isEnabled = false
		btnLogin.isEnabled = false
	}

	private func hideProgress() {
		progressIndicator.stopAnimation(nil)
		fieldEmail.isEnabled = true
		fieldPassword.isEnabled = true
		btnLogin.isEnabled = true
	}

	//MARK: Actions
	@IBAction func actLogin(_ sender: BaseButton) {

		sender.runTapAnimation()
		
		//get credentials
		let input = LoginInput.init(email: fieldEmail.stringValue,
									password: fieldPassword.stringValue)

		//Validate credentials
		guard isValid(data: input) else {
			return
		}

		//call login api
		login(withCredentials: input)

	}

	@IBAction func actSignup(_ sender: BaseButton) {
		sender.runTapAnimation()
		guard let url = URL(string: "http://technodevs.com") else { return }
		NSWorkspace.shared.open(url)
	}

	//MARK: Data
	private func isValid(data: LoginInput) -> Bool {

		var message = ""

		//validate email
		if !data.email.isValid(forExp: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}") {
			message = "Invalid Email"
		}else if data.password.isEmpty {
			message.append("Password cannot be empty")
		}

		errorLabel.stringValue = message

		if message.isEmpty {
			return true
		}else {
			return false
		}
	}

	//MARK: APi
	private func login(withCredentials credentials: LoginInput) {
		showProgress()
		let request = TechnoDevsApi.login(credentials)
		TechnoDevsApi.provider.request(request) { [weak self] (result) in
			guard let self = self else { return }
			self.hideProgress()
			do {
				let response = try result.get().filterSuccessfulStatusCodes()
				let responseStatus = try response.map(ResponseStatus.self)

				//response is success
				if responseStatus.success {
					let loginResponse = try response.map(LoginResponse.self)
					LoginSession.current.set(loginResponse: loginResponse)
					self.showHomeScene()
				}
				//response is failed
				else {
					let error = try response.map(ResponseError.self)
					self.errorLabel.stringValue = error.message.first ?? ""
				}
			} catch {
				//error while parsing
				let moyaError = error as? MoyaError
				self.errorLabel.stringValue = moyaError?.errorDescription ?? ""
			}
		}
	}

	//MARK: Navigation
	private func showHomeScene() {
		let homeScene = HomeViewController.instantiate()
		self.view.window?.contentViewController = homeScene
	}
}

