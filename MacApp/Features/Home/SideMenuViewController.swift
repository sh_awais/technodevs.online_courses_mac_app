//
//  MenuViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 11/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa


protocol SideMenuViewDelegate: class {
	func sideMenuView(didSelectMenu menuType: MenuType)
	func sideMenuView(didUpdateState state: SideMenuViewController.State)
}

class SideMenuViewController: TechnoDevController {

	@IBOutlet weak private var tableView: NSTableView!

	@IBOutlet weak private var userName: NSTextField!
	@IBOutlet weak private var userPhoto: NSImageView!
	@IBOutlet weak private var separator: NSView!

	//containt for animation
	@IBOutlet weak private var photoWidth: NSLayoutConstraint!
	@IBOutlet weak private var photoTop: NSLayoutConstraint!
	@IBOutlet weak private var menuTrailing: NSLayoutConstraint!

	weak var delegate: SideMenuViewDelegate?

	let reuseId = NSUserInterfaceItemIdentifier(rawValue: "SideMenuTableCellView")
	let columnId =  NSUserInterfaceItemIdentifier("MenuInfo")

	enum State {
		case collapse
		case expand
	}

	var menuList: [MenuItem] = [] {
		didSet {
			tableView.reloadData()
		}
	}

	var selectedMenu = MenuType.home
	private var currentState: State = .expand {
		didSet {

			self.delegate?.sideMenuView(didUpdateState: currentState)
			self.updateCells(forState: self.currentState)
			animateSideMenu()
		}
	}

	func getCurrentState() -> State {
		return currentState
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		//user info separtor
		separator.wantsLayer = true
		separator.layer?.backgroundColor = NSColor.lightGray.cgColor
		
		//Configure tableView sources
		tableView.dataSource = self
		tableView.delegate = self

		//Create Menu List
		menuList = MenuItem.fetchMenuList()

		//disable default selection style
		tableView.selectionHighlightStyle = .none

		//fill user info
		fillUserInfo()

//		view.translatesAutoresizingMaskIntoConstraints = false

	}

	//MARK: Animation
	private func animateSideMenu() {

		let photoWidth: CGFloat = (currentState == .expand) ? 100 : 60
		let photoTop: CGFloat = (currentState == .expand) ? 12 : 28
		menuTrailing.constant = (currentState == .expand) ? 20 : 32
		self.photoWidth.constant = photoWidth
		self.photoTop.constant = photoTop
		self.userName.alphaValue = 0

		let position = tableView.column(withIdentifier: columnId)
		let column = tableView.tableColumns[position]
		column.width = (currentState == .expand) ? 250 : 90



		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.view.layoutSubtreeIfNeeded()

		}) { [weak self] in
			guard let self = self else { return }
			NSAnimationContext.runAnimationGroup({ [weak self] (context) in
				context.allowsImplicitAnimation = true
				context.duration = 0.2
				self?.userName.alphaValue =  (self?.currentState == .expand) ? 1.0 : 0
			})
		}
	}

	private func updateCells(forState state: State) {
		for i in 0..<menuList.count {
			let cell = tableView.view(atColumn: 0, row: i, makeIfNecessary: false) as? SideMenuTableCellView
			cell?.configure(menuItem: menuList[i], menuState: state)
			if menuList[i].type == selectedMenu {
				cell?.isHighlight = true
			}else {
				cell?.isHighlight = false
			}
		}
	}

	//MARK: Actions
	@IBAction func actToggleSideMenu(_ sender: NSButton) {
		switch currentState {
		case .collapse:
			currentState = .expand
		case .expand:
			currentState = .collapse
		}
	}

	//MARK: Data
	private func fillUserInfo() {
		userName.stringValue = LoginSession.current.userName
	}

	//MARK: Navigation
	private func showLoginScene() {
		let homeScene = LoginViewController.instantiate()
		let window = NSApplication.shared.windows.first
		window?.contentViewController = homeScene
		window?.makeKey()
	}
    
}

//MARK: NSTableViewDataSource
extension SideMenuViewController: NSTableViewDataSource {
	func numberOfRows(in tableView: NSTableView) -> Int {
		return menuList.count
	}
}

//MARK: NSTableViewDelegate
extension SideMenuViewController: NSTableViewDelegate {

	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		let cell = tableView.makeView(withIdentifier: reuseId,
									  owner: nil) as! SideMenuTableCellView
		let menuItem = menuList[row]

		//Configure cell
		cell.configure(menuItem: menuItem, menuState: currentState)

		if menuItem.type == selectedMenu {
			cell.isHighlight = true
		}else {
			cell.isHighlight = false
		}

		return cell
	}

	func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {

		guard menuList[row].type != .logout  else {

			self.updateSelection(forMenuItem: .logout, shouldSelect: true)
			NSAlert.showLogoutConfirmationAlert { [weak self] (isConfirmed) in
				self?.updateSelection(forMenuItem: .logout, shouldSelect: false)
				guard isConfirmed else { return }
				LoginSession.current.logout()
			}
			return false
		}

		//deselect old selection
		updateSelection(forMenuItem: selectedMenu, shouldSelect: false)

		//update type and selection for new item
		selectedMenu = menuList[row].type
		updateSelection(forMenuItem: selectedMenu, shouldSelect: true)

		delegate?.sideMenuView(didSelectMenu: selectedMenu)
		return true
	}

	private func updateSelection(forMenuItem type: MenuType, shouldSelect: Bool) {
		let row: Int
		switch type {
		case .home:
			row = 0
		case .downloads:
			row = 1
		case .wifiTransfer:
			row = 2
		case .userInfo:
			row = 3
		case .courses:
			row = 4
		case .logout:
			row = 5
		}

		let cell = tableView.view(atColumn: 0, row: row, makeIfNecessary: false) as? SideMenuTableCellView
		cell?.isHighlight = shouldSelect
	}
}


