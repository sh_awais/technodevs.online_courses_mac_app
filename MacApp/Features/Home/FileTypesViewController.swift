//
//  FileTypesViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import SDWebImage

enum FileExtensions: String {
	case mp3 = "mp3"
	case mp4 = "mp4"
	case pdf = "pdf"
	case epub = "epub"
	case unknown = "unknown"

	static func fromString(fileName: String) -> FileExtensions {
		let type = (fileName as NSString).pathExtension
		if type.caseInsensitiveCompare("mp3") == .orderedSame {
			return .mp3
		}else if type.caseInsensitiveCompare("mp4") == .orderedSame {
			return .mp4
		}else if type.caseInsensitiveCompare("pdf") == .orderedSame {
			return .pdf
		}else if type.caseInsensitiveCompare("epub") == .orderedSame {
			return .epub
		}else {
			return .unknown
		}
	}
}

class FileTypesViewController: TechnoDevController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.home

	@IBOutlet weak private var courseTitle: NSTextField!
	@IBOutlet weak private var courseImage: NSImageView!
	@IBOutlet weak private var btnVideo: NSButton!
	@IBOutlet weak private var btnAudio: NSButton!
	@IBOutlet weak private var btnEbook: NSButton!

	var course: URL?
	var repo: HomeRepository?

	//MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

		customizeUI()

		//fill info
		if let courseValue = course {
			fillInfo(course: courseValue)
		}
    }

	private func customizeUI() {
		btnVideo.image = R.image.video.tintedImage(tint: R.color.primary)
		btnAudio.image = R.image.audio.tintedImage(tint: R.color.primary)
		btnEbook.image = R.image.ebook.tintedImage(tint: R.color.primary)
	}

	private func fillInfo(course: URL) {
		courseTitle.stringValue = course.lastPathComponent
		let imgUrl = course.appendingPathComponent("defaultx\(500).png")
		courseImage.image = ImageHelper.shared.image(localUrl: imgUrl)
	}

	//MARK: Actions
	@IBAction func actVideo(_ sender: NSButton) {
		showCourseFilesScene(fileExtensions: [.mp4],
							 breadCrumbTitle: "Video",
							 fileIcon: R.image.video,
							 filesUrl: course?.mediaDirectory(type: .video))
	}

	@IBAction func actAudio(_ sender: NSButton) {
		showCourseFilesScene(fileExtensions: [.mp3],
							 breadCrumbTitle: "Audio",
							 fileIcon: R.image.audio,
							 filesUrl: course?.mediaDirectory(type: .audio))
	}

	@IBAction func actEbook(_ sender: NSButton) {
		showCourseFilesScene(fileExtensions: [.pdf, .epub],
							 breadCrumbTitle: "E-Book",
							 fileIcon: R.image.ebook,
							 filesUrl: course?.mediaDirectory(type: .ebook))
	}

	//MARK: Navigation
	private func showCourseFilesScene(fileExtensions: [FileExtensions],
									  breadCrumbTitle: String,
									  fileIcon: NSImage,
									  filesUrl: URL?) {
		let scene = CourseFilesViewController.instantiate()
		scene.fileExtensions = fileExtensions
		scene.sceneTitle = breadCrumbTitle
		scene.fileIcon = fileIcon
		scene.filesUrl = filesUrl
		scene.courseTitle = courseTitle.stringValue
		scene.repo = repo
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: breadCrumbTitle))
	}
}
