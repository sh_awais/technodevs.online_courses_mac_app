//
//  CourseCategoryCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class HorizontalScrollView: NSScrollView {
	var scrollDirection: NSCollectionView.ScrollDirection = .horizontal
	var currentScrollMatches = false
	override func scrollWheel(with event: NSEvent) {
		self.nextResponder?.scrollWheel(with: event)
	}
}

protocol CourseCategoryCollectionViewItemDelegate: class {
	func courseCategoryCollectionViewItem(didSelectCourseInCategory categoryIndexPath: IndexPath, course: URL)
}

class CourseCategoryCollectionViewItem: NSCollectionViewItem , NibReusable {

	//MARK: Outlets
	@IBOutlet weak private var coursesCollectionView: NSCollectionView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var btnLeft: NSButton!
	@IBOutlet weak private var btnRight: NSButton!

	//MARK: Properties
	private var category: URL? {
		didSet {
			if courses.count <= noOfItemsInPage {
				btnRight.isEnabled = false
			}else {
				btnRight.isEnabled = true
			}
			coursesCollectionView.reloadData()
			headerView.title.stringValue = category?.lastPathComponent ?? ""
		}
	}

	var repo: HomeRepository?

	private var courses: [URL] {
		guard let categoryUrl = category else { return [] }
		return repo?.getCourses(inCategory: categoryUrl) ?? []
	}

	private var count: Int {
		return courses.count
	}

	weak var delegate: CourseCategoryCollectionViewItemDelegate?
	var cellIndexPath: IndexPath?

	private let noOfItemsInPage = 4
	private var currentPosition = 0 {
		didSet {
			// scroll to new position
			let ctx = NSAnimationContext.current
			ctx.allowsImplicitAnimation = true
			let indexSet = Set<IndexPath>(arrayLiteral: IndexPath(item: currentPosition, section: 0))

			//animate to indexSet
			coursesCollectionView.animator().scrollToItems(at: indexSet,
												scrollPosition: NSCollectionView.ScrollPosition.left)

			btnRight.isEnabled = true
			btnLeft.isEnabled = true
			if currentPosition > (count - 1) - noOfItemsInPage {
				btnRight.isEnabled = false
			}else if currentPosition < noOfItemsInPage {
				btnLeft.isEnabled = false
			}
		}
	}

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		coursesCollectionView.dataSource = self
		coursesCollectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()
//		coursesCollectionView.enclosingScrollView?.hasVerticalScroller = false
//		coursesCollectionView.enclosingScrollView?.hasHorizontalScroller = false
		coursesCollectionView.enclosingScrollView?.scrollerInsets = NSEdgeInsets.init(top: 0, left: 0, bottom: -10, right: 0)

		//register item
		coursesCollectionView.register(cellType: CategoryCollectionViewItem.self)

		//set buttons image
		btnLeft.image = btnLeft.image?.tintedImage(tint: R.color.primary)
		btnRight.image = btnRight.image?.tintedImage(tint: R.color.primary)

		btnLeft.isEnabled = false
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewFlowLayout()
		flowLayout.scrollDirection = .horizontal
		flowLayout.sectionInset = NSEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		flowLayout.minimumInteritemSpacing = 0
		flowLayout.minimumLineSpacing = 20
		coursesCollectionView.collectionViewLayout = flowLayout
	}

	//MARK: Actions
	@IBAction private func actLeft(_ sender: NSButton) {
		guard currentPosition > 0 else { return }

		//new position
		var newPosition = currentPosition - noOfItemsInPage

		//in case by noOfItemsInPage new position is less than 0
		if newPosition < noOfItemsInPage {
			newPosition = 0
		}

		//update new position
		currentPosition = newPosition

	}

	@IBAction private func actRight(_ sender: NSButton) {
		guard currentPosition < count else {
			return
		}

		//new position
		var newPosition = currentPosition+noOfItemsInPage

		//in case new position excedes when noOfItemsInPage new item are not available
		if newPosition >= count {
			newPosition = currentPosition + (count - 1 - currentPosition)
		}

		//update new position
		currentPosition = newPosition

	}

	//MARK: Data
	func configure(repo: HomeRepository, category: URL) {
		self.repo = repo
		self.category = category
	}
    
}


//MARK: NSCollectionViewDataSource
extension CourseCategoryCollectionViewItem: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return courses.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {

		let categoryItem: CategoryCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)
	
		let course = courses[indexPath.item]

		//configure item
		categoryItem.configure(course: course)

		return categoryItem
	}
}


//MARK: NSCollectionViewDelegate
extension CourseCategoryCollectionViewItem: NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {

		guard  let catIndexPath = cellIndexPath else { return }

		guard let indexPath = indexPaths.first else { return }

		delegate?.courseCategoryCollectionViewItem(didSelectCourseInCategory: catIndexPath,
												   course: courses[indexPath.item])
		collectionView.deselectItems(at: indexPaths)
	}

	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
//		let width = (self.view.frame.size.width / 4) - (20*4)
//		print(width)

		let cellsAcross: CGFloat = CGFloat(noOfItemsInPage)
		let spaceBetweenCells: CGFloat = 20
		let width = ((self.view.frame.size.width - 42) - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross

		return CGSize(width: width, height: 160)
	}
	
}

