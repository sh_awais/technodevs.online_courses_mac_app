//
//  CategoryCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class CategoryCollectionViewItem: NSCollectionViewItem, NibReusable {

	@IBOutlet weak private var icon: NSImageView!
	@IBOutlet weak private var lblTitle: NSTextField!

	override func viewDidLoad() {
		super.viewDidLoad()
//		self.view.showTestBorder(color: .red)
	}
	
	func configure(course: URL)  {
		let imagePath = course.appendingPathComponent("defaultx\(100)")
			.appendingPathExtension("png")
		icon.image = ImageHelper.shared.image(localUrl: imagePath)

		lblTitle.stringValue = course.lastPathComponent

	}
    
}
