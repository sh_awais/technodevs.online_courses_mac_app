//
//  SideMenuTableCellView.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class SideMenuTableCellView: NSTableCellView {

	@IBOutlet weak var icon: NSImageView!
	@IBOutlet weak var rectangleSelectionImgView: NSImageView!
	@IBOutlet weak var circleSelectionImgView: NSImageView!
	@IBOutlet weak var lblTitle: MenuLabel!

	//constraint for animations
	@IBOutlet weak private var iconLeading: NSLayoutConstraint!


	func configure(menuItem: MenuItem, menuState: SideMenuViewController.State)  {
		icon.image = menuItem.icon
		lblTitle.stringValue = menuItem.title
		configureState(state: menuState)
	}

	private func configureState(state: SideMenuViewController.State) {
		let iconLeadingConstant: CGFloat
		let isHideCircleSelection: Bool
		switch state {
		case .expand:
			iconLeadingConstant = 60
			isHideCircleSelection = true
		case .collapse:
			iconLeadingConstant = 34
			isHideCircleSelection = false
		}


		iconLeading.constant = iconLeadingConstant
		circleSelectionImgView.alphaValue = 1
		rectangleSelectionImgView.alphaValue = 0

		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.layoutSubtreeIfNeeded()

		}) { [weak self] in
			guard let self = self else { return }
			NSAnimationContext.runAnimationGroup({ (context) in
				context.allowsImplicitAnimation = true
				context.duration = 0.15

				self.circleSelectionImgView.alphaValue = isHideCircleSelection ? 0 : 1
				self.rectangleSelectionImgView.alphaValue = isHideCircleSelection ? 1 : 0
			})
		}

	}

	var isHighlight: Bool = false {
		didSet {
			updateHighlightState(shouldSelect: isHighlight)
		}
	}

	private func updateHighlightState(shouldSelect: Bool) {
		if shouldSelect {
			rectangleSelectionImgView.image = R.image.menuSelection
			lblTitle.textColor = R.color.primaryTextLight
			icon.image = icon.image?.tintedImage(tint: R.color.secondary)
			lblTitle.textColor = R.color.primaryTextLight
			circleSelectionImgView.image = R.image.menuSelectionRound
		}else {
			rectangleSelectionImgView.image = nil
			lblTitle.textColor = R.color.primaryTextDark
			icon.image = icon.image?.tintedImage(tint: R.color.secondaryTextDark)
			circleSelectionImgView.image = nil
		}
		lblTitle.isLight = shouldSelect
	}
}
