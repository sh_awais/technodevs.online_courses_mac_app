//
//  HomeViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import CCNNavigationController

class HomeViewController: NSSplitViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.home

	//MARK: Lifecycle

	var existingWindowPosition: CGRect!
	let homeRepo = HomeRepository()
	let coursesRepo = CoursesRepository(token: LoginSession.current.token)

	var menuWidth: NSLayoutConstraint?

	override func viewDidLoad() {
		super.viewDidLoad()

		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.white.cgColor
		
		//set menu delegate
		let menuScene = splitViewItems[0].viewController as? SideMenuViewController
		menuScene?.delegate = self
		splitView.arrangedSubviews[0].translatesAutoresizingMaskIntoConstraints = false
		menuWidth = splitView.arrangedSubviews[0].widthAnchor.constraint(equalToConstant: R.dimension.sideMenuSize.width)
		menuWidth?.isActive = true

		//set content viewController
		splitViewItems[1] = getContentScene(forType: .home)

		self.expandToFulScreen()

		let hideMenuNotification = NSNotification.Name(.hideSideMenu)
		NotificationCenter.default.addObserver(self, selector: #selector(hideSideMenu), name: hideMenuNotification, object: nil)
		let showMenuNotification = NSNotification.Name(.showSideMenu)
		NotificationCenter.default.addObserver(self, selector: #selector(showSideMenu), name: showMenuNotification, object: nil)

	}

	@objc private func hideSideMenu() {
		menuWidth?.constant = 0
		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.view.layoutSubtreeIfNeeded()

		}) { [weak self] in }

	}

	@objc private func showSideMenu() {
		let state = (splitViewItems[0].viewController as? SideMenuViewController)?.getCurrentState() ?? .expand
		menuWidth?.constant = (state == .expand) ? R.dimension.sideMenuSize.width : 90
		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.view.layoutSubtreeIfNeeded()

		}) { [weak self] in }
	}
	
	override func viewDidLayout() {
		super.viewDidLayout()

		splitViewItems[0].viewController.view.layout()
		splitViewItems[1].viewController.view.layout()

	}

	//MARK: Navigation

	// Returns split item for position
	private func getContentScene(forType menuType: MenuType) -> NSSplitViewItem {

		let viewController: NSViewController?
		var firstPageTitle: NSAttributedString?
		let breadCrumItem: BreadCrumItem

		switch menuType {
		case .home:
			viewController = courseCategoriesScene()
			firstPageTitle = homeTitle()
			breadCrumItem = BreadCrumItem(title: "Home")
		case .downloads:
			viewController = downloadManagerScene()
			breadCrumItem = BreadCrumItem(title: "Downloads")
		case .wifiTransfer:
			viewController = wifiTransferScene()
			breadCrumItem = BreadCrumItem(title: "Wifi Transfer")
		case .userInfo:
			viewController = userScene()
			breadCrumItem = BreadCrumItem(title: "User Details")
		case .courses:
			viewController = nonPurchaseCoursesScene()
			breadCrumItem = BreadCrumItem(title: "Courses")
		case .logout:
			viewController = NSViewController()
			breadCrumItem = BreadCrumItem(title: "")
		}


		let navScene = TechnoDevNavigationController(rootViewController: viewController!)
		navScene?.firstPageTitle = firstPageTitle
		navScene?.addBreadCrum(breadCrum: breadCrumItem)

		navScene?.setSize(greaterThanEqualTo: R.dimension.contentSize)
		return NSSplitViewItem(contentListWithViewController: navScene!)
	}

	private func homeTitle() -> NSAttributedString {
		return NSAttributedString.stringFromComponents(firstComponent: "Welcome to", firstFont: R.font.robotoRegular(size: 12), firstTextColor: R.color.primaryTextDark, otherComponent: "Technodevs", otherfont: R.font.robotoRegular(size: 12), otherTextColor: R.color.primary)
	}

	private func courseCategoriesScene() -> CoursesCategoriesViewController {
		let coursesScene = CoursesCategoriesViewController.instantiate()
		coursesScene.repo = homeRepo
		return coursesScene
	}

	private func downloadManagerScene() -> NSViewController {
		let scene =  DownloadsViewController.instantiate()
		scene.repo = coursesRepo
		return scene
	}

	private func userScene() -> NSViewController {
		let scene =  UserViewController.instantiate()
		return scene
	}

	private func nonPurchaseCoursesScene() -> NSViewController {
		let scene =  NonPurchasedCoursesViewController.instantiate()
		scene.repo = coursesRepo
		return scene
	}

	private func wifiTransferScene() -> NSViewController {
		let scene =  WifiTransferViewController.instantiate()
		return scene
	}
}

//MARK: SideMenuViewDelegate
extension HomeViewController: SideMenuViewDelegate {

	func sideMenuView(didSelectMenu menuType: MenuType) {
		splitViewItems[1] = getContentScene(forType: menuType)
	}

	func sideMenuView(didUpdateState state: SideMenuViewController.State) {
		let width = (state == .expand) ? R.dimension.sideMenuSize.width : 90
		self.menuWidth?.constant = width
		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.view.layoutSubtreeIfNeeded()
		}) {
		}
	}
}
