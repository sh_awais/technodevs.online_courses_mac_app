//
//  CoursesViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 12/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class CoursesCategoriesViewController: TechnoDevController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.home

	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var emptyView: NSView!

	//MARK: Properties
	var categories = [URL]()
	var repo: HomeRepository?
	
	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()
		collectionView.enclosingScrollView?.hasHorizontalScroller = false

		//register item
		collectionView.register(cellType: CourseCategoryCollectionViewItem.self)

		//UI
		customizeUI()

		categories = repo?.getCategories() ?? []

		if categories.isEmpty {
			emptyView.isHidden = false
		}else {
			emptyView.isHidden = true
		}
	}

	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "Dashboard"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewFlowLayout()
		flowLayout.scrollDirection = .vertical
		flowLayout.sectionInset = NSEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		flowLayout.minimumInteritemSpacing = 0
		flowLayout.minimumLineSpacing = 20
		collectionView.collectionViewLayout = flowLayout
	}

	//MARK: Navigation
	func showFileTypesScene(course: URL) {
		let scene = FileTypesViewController.instantiate()
		scene.repo = repo
		scene.course = course
		self.navigationController.pushViewController(scene, animated: true)
		(self.navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: course.lastPathComponent))
	}
}


//MARK: NSCollectionViewDataSource
extension CoursesCategoriesViewController: NSCollectionViewDataSource {
	
	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return categories.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {

		let courseCategoryItem: CourseCategoryCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)
		courseCategoryItem.delegate = self
		courseCategoryItem.cellIndexPath = indexPath

		print(categories[indexPath.item].lastPathComponent)
		//configure item
		courseCategoryItem.configure(repo: repo ?? HomeRepository(), category: categories[indexPath.item])

		return courseCategoryItem
	}
}

//MARK: NSCollectionViewDelegate
extension CoursesCategoriesViewController: NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
		collectionView.deselectItems(at: indexPaths)
	}


	func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
		
	}
	
	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
		return CGSize(width: collectionView.frame.size.width, height: 270)
	}
}

//MARK: CourseCategoryCollectionViewItemDelegate
extension CoursesCategoriesViewController: CourseCategoryCollectionViewItemDelegate {
	func courseCategoryCollectionViewItem(didSelectCourseInCategory categoryIndexPath: IndexPath, course: URL) {
		showFileTypesScene(course: course)
	}
}
