//
//  TableOfContentsViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//
import Cocoa

class CourseFilesViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.home

	@IBOutlet weak private var collectionViewContainer: NSView!
	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var emptyFilesView: NSView!
	@IBOutlet weak private var headerView: HeaderView!
	@IBOutlet weak private var courseCount: NSTextField!
	@IBOutlet weak private var lectureTitle: NSTextField!

	var courseFiles = [URL]()
	var filesUrl: URL?
	var repo: HomeRepository?

	var sceneTitle: String = ""
	var courseTitle: String = ""
	var fileExtensions = [FileExtensions]()
	var fileIcon: NSImage?
	
	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: CourseFileCollectionViewItem.self)

		//filter files for extension
		if let url = filesUrl, let repo = repo {
			courseFiles = repo.getFiles(forExtensions: fileExtensions, filesUrl: url)
		}

		//update ui
		customizeUI()

		//show message if files are empty
		if courseFiles.isEmpty {
			emptyFilesView.isHidden = false
			collectionView.isHidden = true
		}else {
			emptyFilesView.isHidden = true
			collectionView.isHidden = false
			collectionView.reloadData()
		}
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewGridLayout()
		flowLayout.maximumNumberOfColumns = 1
		flowLayout.minimumItemSize = NSSize(width: collectionView.frame.size.height, height: 70)
		flowLayout.maximumItemSize = NSSize(width: 2000, height: 70)
		collectionView.collectionViewLayout = flowLayout
	}

	//MARK: UI
	private func customizeUI() {
		courseCount.stringValue = headerSubtitle
		headerView.title.stringValue = "Course \(sceneTitle) Content"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		lectureTitle.stringValue = courseTitle

		collectionViewContainer.showTestBorder(color: R.color.separator)
	}

	private var headerSubtitle: String {
		let count = courseFiles.count
		if count > 1 {
			return "\(count) lectures"
		}else {
			return "\(count) lecture"
		}
	}

}

//MARK: NSCollectionViewDataSource
extension CourseFilesViewController: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return courseFiles.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {

		let courseItem: CourseFileCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		//configure item
		let file = courseFiles[indexPath.item]
		courseItem.configure(style: .fileInfo(indexPath.item, file, fileIcon))

		return courseItem
	}

}

//MARK: NSCollectionViewDelegate
extension CourseFilesViewController: NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {

		guard let indexPath = indexPaths.first else { return }

		//Preview
		CourseFile.showFilePreviewScene(navigationController: self.navigationController, filesList: courseFiles, selectedPosition: indexPath.item)
		collectionView.deselectItems(at: indexPaths)

	}
}
