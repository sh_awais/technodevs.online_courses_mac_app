//
//  VideoPlayerViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 02/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import AVKit

class MediaPlayerViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.preview

	@IBOutlet weak private var playerContainer: NSView!
	@IBOutlet weak private var listTitle: NSTextField!
	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var playListWidth: NSLayoutConstraint!
	@IBOutlet weak var playerContainerTop: NSLayoutConstraint!

	@IBOutlet weak var btnShowList: NSButton!
	private var avPlayerView: AVPlayerView!

	var files = [URL]()
	var selectedFilePosition = 0

	var isFullScreen = false

	enum PlayListState {
		case hide
		case show
	}

	var playListState: PlayListState = .show {
		didSet {
			togglePlayList(state: playListState)
		}
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: PlaylistCollectionViewItem.self)

		//UI
		customizeUI()

		if selectedFilePosition < files.count {
			play(file: files[selectedFilePosition])
		}
	}

	override func mouseDown(with event: NSEvent) {
		super.mouseDown(with: event)

		guard event.clickCount == 2 else {
			return
		}


		self.isFullScreen = !self.isFullScreen



		let notificationName: NSNotification.Name
		let isHideBreadCrumb: Bool

		if self.isFullScreen {
			self.togglePlayList(state: .hide)
			notificationName = NSNotification.Name(.hideSideMenu)
			self.playerContainerTop.constant = 0
			isHideBreadCrumb = true
			self.btnShowList.isHidden = true
		}else {
			self.togglePlayList(state: self.playListState)
			notificationName = NSNotification.Name(.showSideMenu)
			NotificationCenter.default.post(name: notificationName, object: nil, userInfo: nil)
			self.playerContainerTop.constant = 60
			isHideBreadCrumb = false
			self.btnShowList.isHidden = false
		}
		NotificationCenter.default.post(name: notificationName, object: nil, userInfo: nil)
		(self.navigationController as? TechnoDevNavigationController)?.toggeBreadCrumb(isHide: isHideBreadCrumb)

		DispatchQueue.main.asyncAfter(deadline: .now()+0.3) { [weak self] in
			guard let self = self else { return }
			let isWindowFullscreen = self.view.window!.styleMask.contains(.fullScreen)
			if self.isFullScreen && !isWindowFullscreen{
				self.view.window?.toggleFullScreen(self)
			}else if !self.isFullScreen && isWindowFullscreen {
				self.view.window?.toggleFullScreen(self)
			}
		}
	}
	
	override func viewWillDisappear() {
		super.viewWillDisappear()
		avPlayerView.player?.pause()
		NotificationCenter.default.removeObserver(self)
	}

	override func viewWillAppear() {
		super.viewWillAppear()

		avPlayerView.player?.play()
		NotificationCenter.default.addObserver(self, selector: #selector(playNextItem), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
	}

	//MARK: UI
	private func customizeUI() {
		avPlayerView = AVPlayerView(frame: .zero)
		addPlayerView(playerView: avPlayerView, to: playerContainer)
	}

	private func addPlayerView(playerView child: AVPlayerView, to parent: NSView) {

		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			child.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
			parent.trailingAnchor.constraint(equalTo: child.trailingAnchor),
			child.topAnchor.constraint(equalTo: parent.topAnchor),
			parent.bottomAnchor.constraint(equalTo: child.bottomAnchor)
			])
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewGridLayout()
		flowLayout.maximumNumberOfColumns = 1
		flowLayout.minimumItemSize = NSSize(width: collectionView.frame.size.height, height: 50)
		flowLayout.maximumItemSize = NSSize(width: 2000, height: 50)
		collectionView.collectionViewLayout = flowLayout
	}

	//MARK: Actions
	@IBAction func actTogglePlaylist(_ sender: NSButton) {
		playListState = .hide
	}

	@IBAction func actShowPlaylist(_ sender: NSButton) {
		playListState = .show
	}

	private func togglePlayList(state: PlayListState) {
		let width: CGFloat
		switch state {
		case .hide:
			width = 0
		case .show:
			width = 200
		}

		playListWidth.constant = width
		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.3
			self?.view.layoutSubtreeIfNeeded()
		}) {
		}
	}

	//MARK: Data
	private func play(file: URL) {
		let player = AVPlayer(url: file)
		avPlayerView.player = player

		player.play()
	}

	@objc private func playNextItem() {
		let nextItem = selectedFilePosition + 1
		guard nextItem < files.count else { return }

		let url = files[nextItem]
		play(file: url)

		updateCell(atIndex: IndexPath(item: selectedFilePosition, section: 0), isSelected: false)
		updateCell(atIndex: IndexPath(item: nextItem, section: 0), isSelected: true)

		selectedFilePosition = nextItem
	}
}

//MARK: NSCollectionViewDataSource
extension MediaPlayerViewController: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return files.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {

		let itemView: PlaylistCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		//configure item
		itemView.configure(file: files[indexPath.item])
		if indexPath.item == selectedFilePosition {
			itemView.icon.image = R.image.playListItem.tintedImage(tint: R.color.primary)
		}else {
			itemView.icon.image = R.image.playListItem
		}

		return itemView
	}
}

//MARK: NSCollectionViewDelegate
extension MediaPlayerViewController: NSCollectionViewDelegate {

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {

		if let indexPath = indexPaths.first	 {
			updateCell(atIndex: IndexPath(item: selectedFilePosition, section: 0), isSelected: false)
			updateCell(atIndex: indexPath, isSelected: true)
			play(file: files[indexPath.item])
			selectedFilePosition = indexPath.item
		}
		collectionView.deselectAll(nil)
	}

	private func updateCell(atIndex index: IndexPath, isSelected: Bool) {
		let cell = collectionView.item(at: index) as? PlaylistCollectionViewItem
		let img = isSelected ? R.image.playListItem.tintedImage(tint: R.color.primary) : R.image.playListItem
		cell?.icon.image = img
	}
}
