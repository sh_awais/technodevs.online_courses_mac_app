//
//  PdfReaderViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 02/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import WebKit
import EPUBKit

class EpubReaderViewController: NSPageController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.preview

	private var chapters = [EPUBManifestItem]() {
		didSet {
			var indexes = [String]()
			for index in 0 ..< chapters.count {
				indexes.append("\(index)")
			}
			guard !indexes.isEmpty else { return }
			self.arrangedObjects = indexes

//			DispatchQueue.main.asyncAfter(deadline: .now()+1.0) {
				self.changePage(page: 1)
//			}
		}
	}
	
	private var book: EPUBDocument? {
		didSet {
			guard book != nil else { return }
			var chaps = [EPUBManifestItem]()
			for spineItem in book!.spine.items {
				if let item = book!.manifest.items[spineItem.idref] {
					chaps.append(item)
				}
			}
			chapters = chaps
		}
	}

	var fileUrl: URL?

	var webViewController: WebViewController!

	@IBOutlet weak var lblMessage: NSTextField!
	@IBOutlet weak var progressIndicator: NSProgressIndicator!

	//MARK: Lifecycle
	/*
	override func loadView() {
		self.view = NSView(frame: .zero)

	}
	
	*/
	override func viewDidLoad() {
		super.viewDidLoad()


		webViewController = WebViewController.instantiate()
		webViewController.delegate = self

		self.delegate = self
		self.transitionStyle = .horizontalStrip

		guard let url = fileUrl else {
			self.progressIndicator.stopAnimation(nil)
			lblMessage.isHidden = false
			return
		}

		progressIndicator.startAnimation(nil)
		loadBook(url: url) { [weak self] (result) in
			self?.progressIndicator.stopAnimation(nil)
			if let newBook = result {
				self?.book = newBook
			}else {
				self?.lblMessage.isHidden = false
			}
		}
	}

	/*
	private func showProgressIndicator() {
		progressIndicator.startAnimation(nil)
		progressIndicator.isHidden = false
	}

	private func showProgressIndicator() {

		progressIndicator.startAnimation(nil)
		progressIndicator.isHidden = false
	}
	*/

	private func loadBook(url: URL, completion:@escaping (EPUBDocument?) -> Void) {
		DispatchQueue.global(qos: .background).async {
			let book = EPUBDocument(url: url)
			DispatchQueue.main.async {
				completion(book)
			}
		}
	}
}

//MARK: NSPageControllerDelegate
extension EpubReaderViewController: NSPageControllerDelegate {

	func changePage(page : Int ){
		NSAnimationContext.runAnimationGroup({ (_) in
			self.animator().selectedIndex = page
		}) {
			self.completeTransition()
		}
	}

	func pageController(_ pageController: NSPageController, identifierFor object: Any) -> String {
		return object as! String
	}

	func pageController(_ pageController: NSPageController, viewControllerForIdentifier identifier: String) -> NSViewController {

		//default view controller
//		guard let index = Int(identifier) else {
//			return NSViewController()
//		}
		let index = Int(identifier)!

		//show cover
		let url: URL?
		let manifestItem = chapters[index]
		url = book?.directory.appendingPathComponent(manifestItem.path)

		//configure webViewController
//		guard url != nil else {
//			return NSViewController()
//		}

		webViewController.webView?.loadFileURL(url!, allowingReadAccessTo: book!.directory)
		webViewController.footnote?.stringValue = "\(self.selectedIndex)/\(chapters.count)"

		//return controller
		return webViewController
	}

	func pageControllerDidEndLiveTransition(_ pageController: NSPageController) {
		self.completeTransition()
	}

}

//MARK: WebViewControllerDelegate
extension EpubReaderViewController: WebViewControllerDelegate {

	func webViewControllerDidTapNext() {
		let newPage = self.selectedIndex + 1
		guard newPage < chapters.count else { return }
		changePage(page: newPage)
	}

	func webViewControllerDidTapPrevious() {
		let newPage = self.selectedIndex - 1
		guard newPage >= 0 else { return }
		changePage(page: newPage)
	}
}
