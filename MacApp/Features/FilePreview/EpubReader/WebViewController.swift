//
//  WebViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 03/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import WebKit

protocol WebViewControllerDelegate {
	func webViewControllerDidTapNext()
	func webViewControllerDidTapPrevious()
}

class CustomWebView: WKWebView {
	var scrollDirection: NSCollectionView.ScrollDirection = .vertical
	var currentScrollMatches = false

	override func scrollWheel(with event: NSEvent) {
		/* Ensure that both scrollbars are flashed when the user taps trackpad with two fingers */
		if (event.phase == NSEvent.Phase.mayBegin) {
			super.scrollWheel(with: event)
			return
		}
		/* Check the scroll direction only at the beginning of a gesture for modern scrolling devices */
		/* Check every event for legacy scrolling devices */
		if (event.phase == NSEvent.Phase.began || (event.phase == [] && event.momentumPhase == [])) {
			switch scrollDirection {
			case .vertical:
				currentScrollMatches = abs(event.scrollingDeltaY) > abs(event.scrollingDeltaX)
			case .horizontal:
				currentScrollMatches = abs(event.scrollingDeltaX) > abs(event.scrollingDeltaY)
			}
		}
		if currentScrollMatches {
			super.scrollWheel(with: event)
		} else {
			self.nextResponder?.scrollWheel(with: event)
		}
	}
}

class WebViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.preview
	/*
	var webView: WKWebView?
	var buttonNext: NSButton?
	var buttonPrevious: NSButton?
	var footnote: NSTextField?
	*/
	@IBOutlet var webView: WKWebView?
	@IBOutlet var buttonNext: NSButton?
	@IBOutlet var buttonPrevious: NSButton?
	@IBOutlet var footnote: NSTextField?

	var delegate: WebViewControllerDelegate?

//	override func loadView() {
//		self.view = NSView(frame: .zero)
//	}

	override func viewDidLoad() {
		super.viewDidLoad()

//		customizeUI()

		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.clear.cgColor

	}

	//MARK: UI
	private func customizeUI() {

		webView = WKWebView(frame: .zero)
		addWebView(webView: webView!)


		buttonNext = NSButton(frame: .zero)
		buttonNext?.target = self
		buttonNext?.action = #selector(actNext(_:))
		buttonNext?.title = "Next"
		addButton(btnNext: buttonNext!)

		buttonPrevious = NSButton(frame: .zero)
		buttonPrevious?.action = #selector(actPrevious(_:))
		buttonPrevious?.title = "Previous"
		buttonPrevious?.target = self
		addButton(btnPrevious: buttonPrevious!)


		footnote = NSTextField(frame: .zero)
		footnote?.stringValue = "0/0"
		footnote?.isEditable = false
		footnote?.alignment = .center
		footnote?.textColor = NSColor.black
		addFootnote(textField: footnote!)
	
	}

	private func addWebView(webView: WKWebView) {
		let parent = self.view

		webView.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(webView)

		NSLayoutConstraint.activate([
			webView.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: 20),
			parent.trailingAnchor.constraint(equalTo: webView.trailingAnchor, constant: 20),
			webView.topAnchor.constraint(equalTo: parent.topAnchor, constant: 60),
			parent.bottomAnchor.constraint(equalTo: webView.bottomAnchor, constant: 100)
			])
	}

	private func addButton(btnNext: NSButton) {
		let parent = self.view

		btnNext.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(btnNext)

		NSLayoutConstraint.activate([
			btnNext.centerXAnchor.constraint(equalTo: parent.centerXAnchor,
											 constant: 100),
			parent.bottomAnchor.constraint(equalTo: btnNext.bottomAnchor, constant: 16)
			])
	}

	private func addButton(btnPrevious: NSButton) {
		let parent = self.view

		btnPrevious.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(btnPrevious)

		NSLayoutConstraint.activate([
			btnPrevious.centerXAnchor.constraint(equalTo: parent.centerXAnchor,
												 constant: -100),
			parent.bottomAnchor.constraint(equalTo: btnPrevious.bottomAnchor, constant: 16)
			])
	}

	private func addFootnote(textField: NSTextField) {
		let parent = self.view

		textField.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(textField)

		NSLayoutConstraint.activate([
			textField.widthAnchor.constraint(equalToConstant: 100),
			textField.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
			parent.bottomAnchor.constraint(equalTo: textField.bottomAnchor, constant: 16)
			])
	}

	//MARK: Actions
	/*
	@objc private func actNext(_ sender: NSButton) {
		delegate?.webViewControllerDidTapNext()
	}

	@objc private func actPrevious(_ sender: NSButton) {
		delegate?.webViewControllerDidTapPrevious()
	}
	*/
	@IBAction private func actNext(_ sender: NSButton) {
		delegate?.webViewControllerDidTapNext()
	}

	@IBAction private func actPrevious(_ sender: NSButton) {
		delegate?.webViewControllerDidTapPrevious()
	}
}
