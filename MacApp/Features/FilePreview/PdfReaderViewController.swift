//
//  PdfReaderViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 02/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import WebKit

class PdfReaderViewController: NSViewController {

	private var webView: WKWebView!

	var fileUrl: URL?

	override func loadView() {
		self.view = NSView(frame: .zero)
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do view setup here.
		guard let url = fileUrl else {
			return
		}

		customizeUI()

		webView.loadFileURL(url, allowingReadAccessTo: url)

    }

	//MARK: UI
	private func customizeUI() {
		webView = WKWebView(frame: .zero)
		addWebView(webView: webView)
//		addBackButton()
	}

	private func addWebView(webView: WKWebView) {
		let parent = self.view

		webView.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(webView)

		NSLayoutConstraint.activate([
			webView.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
			parent.trailingAnchor.constraint(equalTo: webView.trailingAnchor),
			webView.topAnchor.constraint(equalTo: parent.topAnchor, constant: 60),
			parent.bottomAnchor.constraint(equalTo: webView.bottomAnchor)
			])
	}

	/*
	private func addBackButton() {
		let button = NSButton(frame: .zero)
		button.image = R.image.arrowLeft
		button.target = self
		button.action = #selector(actBack)
		button.bezelStyle = .texturedRounded

		button.translatesAutoresizingMaskIntoConstraints = false
		self.view.addSubview(button)

		NSLayoutConstraint.activate([
			button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 12),
			button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20),
			button.widthAnchor.constraint(equalToConstant: 23),
			button.heightAnchor.constraint(equalToConstant: 23),
		])
	}

	@objc private func actBack() {
		self.navigationController.popViewController(animated: true)
	}
	*/
}

