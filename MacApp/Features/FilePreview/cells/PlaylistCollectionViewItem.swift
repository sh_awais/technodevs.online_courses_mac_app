//
//  PlaylistCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class PlaylistCollectionViewItem: NSCollectionViewItem, NibReusable {

	@IBOutlet weak var icon: NSImageView!
	@IBOutlet weak var itemTitle: NSTextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }

	func configure(file: URL) {
		itemTitle.stringValue = file.lastPathComponent
	}
}
