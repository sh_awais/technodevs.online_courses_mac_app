//
//  NonPurchasedCoursesViewController.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class NonPurchasedCoursesViewController: NSViewController, StoryboardSceneBased {
	static var sceneStoryboard: NSStoryboard = R.storyboard.nonPurchasedCourses

	@IBOutlet weak private var collectionViewContainer: NSView!
	@IBOutlet weak private var collectionView: NSCollectionView!
	@IBOutlet weak private var headerView: HeaderView!

	///Api Status
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!
	@IBOutlet weak private var errorView: NSView!
	@IBOutlet weak private var errorLabel: NSTextField!
	@IBOutlet weak private var btnReload: NSButton!
	@IBOutlet weak private var apiStatusView: NSView!

	var repo: CoursesRepository!
	let loginTitle = "Login"
	let reloadTitle = "Reload"
	var courses = [Course]()

	//MARK: Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()

		//Configure collectionView sources
		collectionView.dataSource = self
		collectionView.delegate = self

		//configure collectionViewLayout
		configureCollectionViewLayout()

		//register item
		collectionView.register(cellType: CourseDownloadCollectionViewItem.self)

		//update ui
		customizeUI()

	}

	override func viewWillAppear() {
		super.viewWillAppear()

		fetchCourses()
	}

	//MARK: UI
	private func customizeUI() {
		headerView.title.stringValue = "Courses"
		headerView.set(font: R.font.robotoRegular(size: 18), color: R.color.primaryTextDark)
		headerView.set(highlightViewHeight: 3)

		collectionViewContainer.showTestBorder(color: R.color.separator)

		apiStatusView.wantsLayer = true
		apiStatusView.layer?.backgroundColor = NSColor.white.cgColor
	}

	private func configureCollectionViewLayout() {
		let flowLayout = NSCollectionViewFlowLayout()
		flowLayout.scrollDirection = .vertical
		collectionView.collectionViewLayout = flowLayout
	}

	private func showProgress() {
		hideError()
		progressIndicator.startAnimation(nil)
		apiStatusView.isHidden = false
	}

	private func hideProgress() {
		progressIndicator.stopAnimation(nil)
		apiStatusView.isHidden = true
	}

	private func showError(error: String) {
		errorLabel.stringValue = error
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func showEmptyView() {
		errorLabel.stringValue = "Courses are empty"
		hideProgress()
		errorView.isHidden = false
		apiStatusView.isHidden = false
	}

	private func hideError() {
		errorView.isHidden = true
		apiStatusView.isHidden = true
	}


	//MARK: Actions
	@IBAction private func actReload(_ sender: NSButton) {
		if sender.title == loginTitle {
			NSAlert.showLogoutConfirmationAlert { (isConfirmed) in
				guard isConfirmed else { return }
				LoginSession.current.logout()
			}
		}else {
			fetchCourses()
		}
	}

	//MARK: Api
	private func fetchCourses() {
		showProgress()
		repo.fetchNonPurchasedCourses { [weak self] (result) in
			switch result {
			case .success(let courses):
				guard let self = self else { return }
				self.courses = courses
				self.hideProgress()
				self.reloadData()
			case .failure(let error):
				guard let self = self else { return }
				if error.localizedDescription == R.apiError.sessionExpired {
					self.btnReload.title = self.loginTitle
				}else {
					self.btnReload.title = self.reloadTitle
				}
				self.showError(error: error.localizedDescription)
			}
		}
	}

	private func reloadData() {
		if courses.isEmpty {
			showEmptyView()
		}else {
			collectionView.reloadData()
		}
	}
}

//MARK: NSCollectionViewDataSource
extension NonPurchasedCoursesViewController: NSCollectionViewDataSource {

	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		return courses.count
	}

	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
		let courseItem: CourseDownloadCollectionViewItem = collectionView.dequeueReusableCell(for: indexPath)

		let course = courses[indexPath.item]

		//configure item
		courseItem.configure(slNo: indexPath.item+1, course: course)
		courseItem.delegate = self

		courseItem.btlDownload.image = R.image.cart.tintedImage(tint: R.color.secondary)
		courseItem.btlDownload.title = "Buy Now"
		courseItem.btlDownload.attributedTitle = courseItem.btlDownload.titleWithColor(color: R.color.primaryTextLight)
		return courseItem
	}
}

//MARK: NSCollectionViewDelegate
extension NonPurchasedCoursesViewController: NSCollectionViewDelegate, NSCollectionViewDelegateFlowLayout {

	func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
		return NSSize(width: collectionView.frame.size.width, height: 70)
	}

	func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
		collectionView.deselectItems(at: indexPaths)
	}
}

//MARK: CourseDownloadCollectionViewItemDelegate
extension NonPurchasedCoursesViewController: CourseDownloadCollectionViewItemDelegate {

	func courseDownloadCollectionViewItem(didTapDownload cell: CourseDownloadCollectionViewItem) {
		guard let indexPath = collectionView.indexPath(for: cell) else { return }
		guard indexPath.item < courses.count else { return }
		let course = courses[indexPath.item]
		
	}
}
