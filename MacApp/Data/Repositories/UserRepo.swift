//
//  UserRepo.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Result
import Moya

class UserRepository {


	init() { }

	func updateUser(input: UserInput, completion: @escaping (Result<LoginResponse,MoyaError>) -> Void) {

		//fetch courses list
		TechnoDevsApi.provider.request(.updateUser(input)) { (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: LoginResponse.self, completion: completion)
		}
	}
}
