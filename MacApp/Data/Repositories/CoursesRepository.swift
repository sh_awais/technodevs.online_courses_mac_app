//
//  DownloadsRepository.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Result
import Moya

class CoursesRepository {

	let token: SessionToken

	init(token: SessionToken) {
		self.token = token
	}
	
	func fetchCourses(completion: @escaping (Result<[Course],MoyaError>) -> Void) {

		//fetch courses list
		TechnoDevsApi.provider.request(.courses(LoginSession.current.token)) { (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: CoursesData.self, completion: { [weak self] (result) in
				switch result {
				case .success(let coursesData):
					let courses = self?.filterEmptyCourses(courses: coursesData.courses) ?? []
					completion(Result.success(courses))
				case .failure(let error):
					completion(Result.failure(error))
				}
			})
		}
	}

	private func filterEmptyCourses(courses: [Course] ) -> [Course] {
		var filteredCourses = [Course]()
		for course in courses {
			if !course.availableFilesId.isEmpty {
				filteredCourses.append(course)
			}
		}
		return filteredCourses
	}

	func fechCourseDetails(courseId: Int, completion: @escaping (Result<Course,MoyaError>) -> Void) {
		TechnoDevsApi.provider.request(.courseDetails(LoginSession.current.token, courseId)) { [weak self] (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: Course.self, completion: { (result) in
				switch result {
				case .success(let course):
					var newCourse = course
					newCourse.files = self?.filterExpiredFiles(files: course.files) ?? []
					completion(Result.success(newCourse))
				case .failure(let error):
					completion(Result.failure(error))
				}
			})
		}
	}

	private func filterExpiredFiles(files: [CourseFile]) -> [CourseFile] {
		var filteredFiles = [CourseFile]()
		for file in files {
			if file.download_count > 0 {
				filteredFiles.append(file)
			}
		}
		return filteredFiles
	}

	///Non-Purchased Courses
	func fetchNonPurchasedCourses(completion: @escaping (Result<[Course],MoyaError>) -> Void) {
		TechnoDevsApi.provider.request(.nonPurchasedCourses(LoginSession.current.token)) { (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: CoursesData.self, completion: { (result) in
				switch result {
				case .success(let coursesData):
					completion(Result.success(coursesData.courses))
				case .failure(let error):
					completion(Result.failure(error))
				}
			})
		}
	}

	///Mark Download Complete
	func markDownloadComplete(fileId: Int, completion: @escaping (Result<ResponseStatus,MoyaError>) -> Void) {
		TechnoDevsApi.provider.request(.downloadComplete(LoginSession.current.token, fileId)) { (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: ResponseStatus.self, completion: { (result) in
				switch result {
				case .success(let responseStatus):
					completion(Result.success(responseStatus))
				case .failure(let error):
					completion(Result.failure(error))
				}
			})
		}
	}
}
