//
//  HomeRepositories.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

class HomeRepository {

	init() { }

	private func subDirectories(inDirectory directory: URL) -> [URL] {
		do {
			let contents = try FileManager.default.contentsOfDirectory(at: directory,
																	   includingPropertiesForKeys: nil,
																	   options: [])
			return contents

		}catch {
			return []
		}
	}

	func getCategories() -> [URL] {
		let url = TechnoDevDirectories.userDirectory
		let categoriesUrl = subDirectories(inDirectory: url)
		return filterEmptyCategories(categories: categoriesUrl)
	}

	private func filterEmptyCategories(categories: [URL]) -> [URL] {
		var filteredCategories = [URL]()
		for category in categories {

			switch category.lastPathComponent {
			case "Uploads":
				if let uploadCategory = uploadsCategory(category: category) {
					filteredCategories.insert(uploadCategory, at: 0)
				}
			default:
				let filteredCourses = filterEmptyCourses(courses: subDirectories(inDirectory: category))
				if !filteredCourses.isEmpty {
					filteredCategories.append(category)
				}
			}
		}
		return filteredCategories
	}

	private func uploadsCategory(category: URL) -> URL? {
		let filteredCourses = filterEmptyCourses(courses: subDirectories(inDirectory: category))
		if !filteredCourses.isEmpty {
			return category
		}
		return nil
	}

	func getCourses(inCategory category: URL) -> [URL] {
		return filterEmptyCourses(courses: subDirectories(inDirectory: category))
	}

	private func filterEmptyCourses(courses: [URL]) -> [URL] {
		var filteredCourses = [URL]()
		for course in courses {
			let isEmptyVideos = subDirectories(inDirectory: course.mediaDirectory(type: .video)).isEmpty
			let isEmptyAudio = subDirectories(inDirectory: course.mediaDirectory(type: .audio)).isEmpty
			let isEmptyEbook = subDirectories(inDirectory: course.mediaDirectory(type: .ebook)).isEmpty
			if !isEmptyEbook || !isEmptyAudio || !isEmptyVideos {
				filteredCourses.append(course)
			}
		}
		return filteredCourses
	}



	func getFiles(forExtensions fileExtensions: [FileExtensions], filesUrl: URL) -> [URL] {
		let files = subDirectories(inDirectory: filesUrl)

		var filteredFiles = [URL]()
		for file in files {
			for fileExtension in  fileExtensions {
				if fileExtension.rawValue.caseInsensitiveCompare(file.pathExtension) == .orderedSame {
					filteredFiles.append(file)
				}
			}
		}
		return filteredFiles
	}
}
