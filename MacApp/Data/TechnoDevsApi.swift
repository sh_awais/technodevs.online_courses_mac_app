//
//  RestApiProvider.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Moya
import Result


enum TechnoDevsApi {
	case login(LoginInput)
	case courses(SessionToken)
	case courseDetails(SessionToken,Int)
	case nonPurchasedCourses(SessionToken)
	case fetchFileDownloadLink(SessionToken,Int)
	case updateUser(UserInput)
	case downloadComplete(SessionToken,Int)

	static let provider = MoyaProvider<TechnoDevsApi>()
}


extension TechnoDevsApi: TargetType {

	public var baseURL: URL { return URL(string: "http://course_online.technodevs.com/api")! }

	public var path: String {
		switch self {
		case .login(_):
			return "/login"
		case .courses(_):
			return "/courses"
		case .courseDetails(_, let courseId):
			return "courses/\(courseId)/download_course"
		case .nonPurchasedCourses(_):
			return "/courses/non-purchased"
		case .fetchFileDownloadLink(_,let fileId):
			return "course-file/\(fileId)/s3-url"
		case .updateUser(_):
			return "/user/update"
		case .downloadComplete(_, let fileId):
			return "/course-file/update/\(fileId)"
		}
	}

	public var method: Moya.Method {
		return .post
	}

	public var task: Task {
		switch self {
		case .login(let input):
			return .requestParameters(parameters: input.json, encoding: URLEncoding.default)
		case .courses(let token):
			return .requestParameters(parameters: token.json, encoding: URLEncoding.default)
		case .courseDetails(let token,_):
			return .requestParameters(parameters: token.json, encoding: URLEncoding.default)
		case .nonPurchasedCourses(let token):
			return .requestParameters(parameters: token.json, encoding: URLEncoding.default)
		case .fetchFileDownloadLink(let token,_):
			return .requestParameters(parameters: token.json, encoding: URLEncoding.default)
		case .updateUser(let input):
			return .requestParameters(parameters: input.json, encoding: URLEncoding.default)
		case .downloadComplete(let token,_):
			return .requestParameters(parameters: token.json, encoding: URLEncoding.default)

		}
	}

	public var sampleData: Data {
		return Data()
	}

	public var headers: [String: String]? {
		return nil
	}

	public static func parseResponse<T>(result: Result<Response, MoyaError>,
										ResponseType: T.Type,
										completion: @escaping(Result<T, MoyaError>) -> Void) where T: Decodable {
		do {
			let response = try result.get().filterSuccessfulStatusCodes()
			let responseStatus = try response.map(ResponseStatus.self)

			//response is success
			if responseStatus.success {
				let mappedModel = try response.map(T.self)
				completion(Result.success(mappedModel))
			}
				//response is failed
			else {
				let customServerError = try response.map(ResponseError.self)
				let firstMessage = customServerError.message.first ?? ""
				let nsError = NSError(domain: "", code: response.statusCode, userInfo: [NSLocalizedDescriptionKey:firstMessage])
				let moyaError = MoyaError.underlying(nsError, response)
				completion(Result.failure(moyaError))
			}
		} catch {
			//error while parsing
			let moyaError: MoyaError
			if let mError = error as? MoyaError {
				moyaError = mError
			}else {
				let nsError = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:""])
				moyaError = MoyaError.underlying(nsError, nil)
			}
			completion(Result.failure(moyaError))
		}
	}

}

