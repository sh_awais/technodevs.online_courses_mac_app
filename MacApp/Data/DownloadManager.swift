//
//  DownloadManager.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Moya
import Alamofire

protocol DownloadManagerDelegate: class {
	func downloadManager(historyUpdated: [CourseFile])
}
class DownloadManager {
	
	static let shared = DownloadManager()

	private var filesList = [String: CourseFile]()
	private var pendlingList = [String: String]()
//	private var inProgressList = [String: Cancellable]()
	private var inProgressRequestList = [Int: DownloadRequest]()
	//	private var inProgressHandlerList = [String: ProgressResponse]()

	private var downloadsInProgressCount: UInt = 0
	
	private let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
	weak var delegate: DownloadManagerDelegate?

	private var isListLoadedFromUserDefaults = false

	let repo = CoursesRepository(token: LoginSession.current.token)
	private init() { }


	private func set(list: [CourseFile]) {
		for file in list {
//			filesList["\(file.id)"] = file
			switch file.downloadStatus {
			case .downloading(_):
				download(file: file)
			//				pendlingList["\(file.id)"] = file.file_name
			default:
				break
			}
		}
	}

	//MARK: Download Limit
	private let keyDownloadLimit = "keyDownloadLimit"
	var downloadLimit: Int {

		set {
			UserDefaults.standard.set(newValue, forKey: keyDownloadLimit)
		}

		get {
			let limit = UserDefaults.standard.integer(forKey: keyDownloadLimit)

			///if limit is not set return 4 which is default limit
			if limit <= 0 {
				return 4
			}
			return limit
		}
	}


	private func fetchDownloadLink(file: CourseFile) {
		//fetch file download link
		CourseFileDownloadLink.fetchFile(token: LoginSession.current.token, fileId: file.id) { (result) in

			switch result {
			case .success(let fileUrl):
				guard let downloadUrl = fileUrl.getUrl() else {
					return
				}

				//download file
				var newFile = file
				newFile.downloadUrl = downloadUrl
				//				newFile.categoryTitle = self.course.category
				//				newFile.courseTitle = self.course.title
				DownloadManager.shared.download(file: newFile)
			case .failure(let error):
				print(error.localizedDescription)
			}
		}
	}

	private func addToQueue(file: CourseFile, downloadLink: URL) {
		var newFile = file

		//get directory url for file
		let localUrl = localLocation(file: file)
		let url = localUrl.createDirectory()!
		//			?? localUrl
		let destination: DownloadRequest.DownloadFileDestination = { _, _ in
			//			var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
			//			documentsURL.appendPathComponent("duck.png")
			return (url, [.removePreviousFile])
		}

		queue.async {

			let request: DownloadRequest
			if let resumeData = self.filesList["\(newFile.id)"]?.resumeData {
				request = Alamofire.download(resumingWith: resumeData, to: destination)
			}else {
				request = Alamofire.download(downloadLink, to: destination)
			}
			//			let request = Alamofire.download(downloadLink, to: destination)
			request.downloadProgress(queue: DispatchQueue.main) { [weak self] (progress) in
				guard progress.completedUnitCount - newFile.completedBytes >= 1 else { return }
				newFile.totalBytes = progress.totalUnitCount
				newFile.completedBytes = progress.completedUnitCount
				newFile.downloadStatus = .downloading(progress.fractionCompleted * 100)
				self?.progressUpdateCallback(file: newFile)
				}.response { (downloadResponse) in
					DispatchQueue.main.async {


						if downloadResponse.error == nil {
							//updates file status
							newFile.downloadStatus = .downloaded
							newFile.download_count -= 1

							self.downloadCompletedCallback(file: newFile)
							//download next pending file

							///Mark file completed
							self.repo.markDownloadComplete(fileId: newFile.id, completion: { (result) in
								switch result {
								case .success(let status):
									print(status.success)
								case .failure(let error):
									print(error.localizedDescription)
								}
							})
							self.startNextPendingFile()

						}else {
							let status = self.filesList["\(newFile.id)"]?.downloadStatus ?? .failed
							switch status {
							case .paused(_):
								newFile.resumeData = downloadResponse.resumeData
								newFile.downloadStatus = .paused(newFile.downloadStatus.progress)
								self.downloadPausedCallback(file: newFile)
							case .failed:
								self.downloadCanceledCallback(file: newFile)
							default:
								newFile.downloadStatus = .failed
								newFile.resumeData = nil
								self.downloadFailedCallback(file: newFile)
							}
							//download next pending file
							self.startNextPendingFile()
						}
					}
			}

			DispatchQueue.main.async { [weak self] in
				self?.inProgressRequestList[newFile.id] = request
			}
		}
	}

	private func progressUpdateCallback(file: CourseFile) {
		//progress update notification
		let notificationName = NSNotification.Name(.fileProgressUpdated)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}

	private func downloadCompletedCallback(file: CourseFile) {

		downloadResponseCallback(file: file, isCompleted: true)

		//post file downloaded notification
		let notificationName = NSNotification.Name(.fileDownloadCompleted)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}

	private func downloadFailedCallback(file: CourseFile) {

		//remove incomplete file
		let url = localLocation(file: file)
		removeIncompleteFile(url: url)

		downloadResponseCallback(file: file)

		//post file downloaded notification
		let notificationName = NSNotification.Name(.fileDownloadingFailed)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}

	private func downloadPausedCallback(file: CourseFile) {
		//remove incomplete file
		let url = localLocation(file: file)
		removeIncompleteFile(url: url)

		//remove from pending list
		downloadResponseCallback(file: file)

		//post file downloaded notification
		let notificationName = NSNotification.Name(.fileDownloadingPaused)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}

	private func downloadCanceledCallback(file: CourseFile) {
		//remove incomplete file
		let url = localLocation(file: file)
		removeIncompleteFile(url: url)

		//remove from pending list
		downloadResponseCallback(file: file, isCompleted: true)

		//post file downloaded notification
		let notificationName = NSNotification.Name(.fileDownloadingCanceled)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}

	/// Decrement downloadsInProgressCount, Update file in list
	///
	/// - Parameter file: updated file
	private func downloadResponseCallback(file: CourseFile, isCompleted: Bool = false) {
		//update file in list
		if isCompleted {
			self.filesList.removeValue(forKey: "\(file.id)")
		}else {
			self.filesList["\(file.id)"] = file
		}

		//send updated progress, remove from pendling list
		if self.downloadsInProgressCount > 0 {
			self.downloadsInProgressCount -= 1
		}
		self.pendlingList.removeValue(forKey: "\(file.id)")
//		self.inProgressList.removeValue(forKey: "\(file.id)")
		self.inProgressRequestList.removeValue(forKey: file.id)
	}

	private func removeIncompleteFile(url: URL) {
		do {
			try FileManager.default.removeItem(at: url)
		}catch {
			print(error.localizedDescription)
		}
	}
	/// starts next pending file download if pending list is no empty
	private func startNextPendingFile() {
		for key in pendlingList.keys {
			if let file = filesList[key] {
				self.download(file: file)
				break
			}
		}
	}
	
	private func localLocation(file: CourseFile) -> URL {
		let courseDirectory = TechnoDevDirectories.userDirectory
			.appendingPathComponent("\(file.categoryTitle ?? "" )")
			.appendingPathComponent("\(file.courseTitle ?? "" )")
		
		let mediaDirectory = courseDirectory.mediaDirectory(fileExtension: file.fileExtension)
		let fileDirectory = mediaDirectory.appendingPathComponent("\(file.fileNameWithoutExtension)")
			.appendingPathExtension("\(file.fileExtension.rawValue)")
		return fileDirectory
	}
}

//MARK: Public Api
extension DownloadManager {
	//MARK: Public Interface
	func loadHistory() {
		set(list: read())
		isListLoadedFromUserDefaults = true
		delegate?.downloadManager(historyUpdated: getList())
	}

	func getList() -> [CourseFile] {
		if isListLoadedFromUserDefaults{
			return Array(filesList.values)
		}else {
			return []
		}
	}

	//MARK: Downloading File

	/// Download a file from url
	///
	/// - Parameters:
	///   - file: file to be downloaded
	func download(file: CourseFile) {


		//skip if file is in progress
		guard inProgressRequestList[file.id] == nil else {
			return
		}

		guard let downloadLink = file.downloadUrl else {
			fetchDownloadLink(file: file)
			return
		}

		//add file to list
		var newFile = file
		newFile.downloadStatus = .downloading(0)
		filesList["\(file.id)"] = newFile

		//check if max allowed limit is reached
		if downloadsInProgressCount >= downloadLimit {
			pendlingList["\(file.id)"] = file.file_name
			return
		}

		//increment download count, Start file download
		downloadsInProgressCount += 1
		addToQueue(file: newFile, downloadLink: downloadLink)

		delegate?.downloadManager(historyUpdated: getList())
	}

	func download(coursesList: [Course])  {
		let syncQueue = DispatchQueue.global(qos: .default)
		syncQueue.sync {
			for course in coursesList {
				fetchCourseDetails(courseId: course.id, completion: { [weak self] (courseDetails) in
					self?.download(course: courseDetails)
				})
			}
		}
	}

	private func fetchCourseDetails(courseId: Int, completion:@escaping (Course) -> Void)  {
		TechnoDevsApi.provider.request(.courseDetails(LoginSession.current.token, courseId)) { (result) in
			TechnoDevsApi.parseResponse(result: result, ResponseType: Course.self, completion: { (result) in
				switch result {
				case.success(let course):
					DispatchQueue.main.async {
						completion(course)
					}
				case .failure(_):
					break
				}
			})
		}
	}

	func download(course: Course)  {
		for file in course.files {
			let url = localLocation(file: file)
			if !FileManager.default.fileExists(atPath: url.absoluteString) {
				var newFile = file
				newFile.courseTitle = course.title
				newFile.categoryTitle = course.category
				download(file: newFile)
			}
		}
	}

	func pause(file: CourseFile)  {

		var newFile = file
		newFile.downloadStatus = FileDownloadStatus.paused(newFile.downloadStatus.progress)
		filesList["\(file.id)"]?.downloadStatus = .paused(newFile.downloadStatus.progress)

		if let request = inProgressRequestList[newFile.id] {
			request.cancel()
		}else {
			newFile.downloadStatus = .failed
			downloadFailedCallback(file: file)
		}
	}

	func play(file: CourseFile)  {

		//		var newFile = file
		//		newFile.downloadStatus = FileDownloadStatus.paused(newFile.downloadStatus.progress)

		//cancel download
		//		let cancellable = inProgressList["\(newFile.id)"]
		//		cancellable?.cancel()
		//		let progress = inProgressHandlerList["\(newFile.id)"]
		//		print(progress?.progressObject?.isPausable)
		//		if progress?.progressObject?.isPausable ?? false {
		//			progress?.progressObject?.pause()
		//		}
		//		if let data = filesList["\(file.id)"]?.resumeData {
		download(file: file)
		//		}else {
		//			var newFile = file
		//			newFile.downloadStatus = .failed
		//			downloadFailedCallback(file: newFile)
		//			startNextPendingFile()
		//		}
	}

	func cancel(file: CourseFile) {

		filesList["\(file.id)"]?.downloadStatus = .failed

		let request = inProgressRequestList[file.id]
		request?.cancel()

		//		let request = inProgressRequestList["\(file.id)"]
		//		request?.cancel()

		//remove incomplete file
		let url = localLocation(file: file)
		removeIncompleteFile(url: url)

		//remove from pending list
		downloadResponseCallback(file: file, isCompleted: true)

		//post file downloaded notification
		let notificationName = NSNotification.Name(.fileDownloadingCanceled)
		NotificationCenter.default.post(name: notificationName, object: file, userInfo: nil)
	}
}

//MARK: CRUD
extension DownloadManager {

	private var keyDownloadsHistory: String {
		return "keyDownloadsHistory"
	}

	func save() {
		let data = getList().jsonEncodedData
		UserDefaults.standard.set(data, forKey: keyDownloadsHistory)
		UserDefaults.standard.synchronize()
	}

	private func read() -> [CourseFile] {
		guard let data = UserDefaults.standard.data(forKey: keyDownloadsHistory) else {
			return []
		}

		let courseFiles: [CourseFile]
		do {
			courseFiles = try JSONDecoder().decode([CourseFile].self, from: data)
		}catch {
			courseFiles = []
		}
		return courseFiles
	}

	func delete() {
		for (_,file) in filesList {
			cancel(file: file)
		}
		filesList = [:]
		pendlingList = [:]
//		inProgressList = [:]
		inProgressRequestList = [:]

		UserDefaults.standard.removeObject(forKey: keyDownloadsHistory)
		UserDefaults.standard.synchronize()
	}
}
