//
//  FileApi.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Moya

enum FileApi {

	case download(url: URL, fileDestinationUrl: URL)

	func fileDestination(fileDestinationUrl: URL) -> DownloadDestination {

		return { _, _ in return (fileDestinationUrl, [.removePreviousFile, .createIntermediateDirectories]) }
	}

	static let provider = MoyaProvider<FileApi>()
}

extension FileApi: TargetType {
	var baseURL: URL {
		switch self {
		case .download(let url, _):
			return url
		}
	}

	var path: String {
		return ""
	}

	var method: Moya.Method {
		return .get
	}

	var sampleData: Data {
		return Data()
	}

	var task: Task {
		switch self {
		case .download(_, let fileDestinationUrl):
			return .downloadDestination(fileDestination(fileDestinationUrl: fileDestinationUrl))
			
		}
	}

	var headers: [String : String]? {
		return nil
	}

	
}
