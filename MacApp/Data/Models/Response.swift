//
//  ErrorReponse.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

struct ResponseStatus: Decodable {
	var success: Bool {
		return _success ?? true
	}
	private var _success: Bool?

	enum CodingKeys: String,CodingKey {
		case _success = "success"
	}
}

struct ResponseError: Decodable {
	var message: [String] = []
}

struct ResponseMessage: Error {
	var message: String = ""

	init() { }
	
	init(message: String) {
		self.message = message
	}
}
