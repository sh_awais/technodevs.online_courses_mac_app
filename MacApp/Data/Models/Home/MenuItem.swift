//
//  ModelItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 11/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

struct MenuItem {
	var title: String
	var icon: NSImage?
	var type: MenuType

	static func fetchMenuList() -> [MenuItem] {
		let home = MenuItem(title: "Home", icon: R.image.home, type: .home)
		let downloads = MenuItem(title: "Downloads", icon: R.image.downloads, type: .downloads)
		let wifiTransfer = MenuItem(title: "Wifi Transfer", icon: R.image.wifitransfer, type: .wifiTransfer)
		let userInfo = MenuItem(title: "User Info", icon: R.image.userInfo, type: .userInfo)
		let courses = MenuItem(title: "Courses", icon: R.image.courses, type: .courses)
		let logout = MenuItem(title: "Logout", icon: R.image.logout, type: .logout)
		return [home,downloads,wifiTransfer,userInfo,courses,logout]
	}
}

enum MenuType {
	case home
	case downloads
	case wifiTransfer
	case userInfo
	case courses
	case logout
}
