//
//  CourseFileLink.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Moya
import Result

struct CourseFileDownloadLink: Decodable {

	var url = ""

	func getUrl() -> URL? {
		return URL(string: url)
	}

	static func fetchFile(token: SessionToken,
						  fileId: Int,
						  completion: @escaping(Result<CourseFileDownloadLink,ResponseMessage>)-> Void) {

		TechnoDevsApi.provider.request(.fetchFileDownloadLink(token, fileId)) {  (result) in
			do {
				let response = try result.get().filterSuccessfulStatusCodes()
				let responseStatus = try response.map(ResponseStatus.self)

				//response is success
				if responseStatus.success {
					let fileLink = try response.map(CourseFileDownloadLink.self)
					completion(Result.success(fileLink))
				}
					//response is failed
				else {
					let error = try response.map(ResponseError.self)
					let message = ResponseMessage.init(message: error.message.first ?? "")
					completion(Result.failure(message))
				}
			} catch {
				//error while parsing
				let moyaError = error as? MoyaError
				let message = ResponseMessage.init(message: moyaError?.errorDescription ?? "")
				completion(Result.failure(message))
			}
		}
	}
}
