//
//  CourseFile.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import CCNNavigationController
import EPUBKit

struct CourseFile: Codable {

	var id: Int = 0
	var file_name: String = ""
	var content_type: String = ""
	var download_count: Int = 0

	///these properties are added manually
	var downloadUrl: URL? 
	var categoryTitle: String?
	var courseTitle: String?
//	var courseIndex: Int?
//	var fileIndex: Int?
	var resumeData: Data?

	///tells files bytes when file is downloading
	var totalBytes: Int64 = 0
	var completedBytes: Int64 = 0

	///tells if user has started dowanlod
	private var _downloadStatus: Int?
	private var _downloadProgress: Double?

	enum CodingKeys: String, CodingKey {
		case id
		case file_name
		case content_type
		case download_count
		case downloadUrl = "fileDownloadLink"
		case categoryTitle
		case courseTitle
		case downloadProgress
		case downloadStatus

	}

	/// Encodes model to json
	public func encode(to encoder: Encoder) throws {

		var container = encoder.container(keyedBy: CodingKeys.self)

		try container.encodeIfPresent(id, forKey: .id)
		try container.encodeIfPresent(file_name, forKey: .file_name)
		try container.encodeIfPresent(content_type, forKey: .content_type)
		try container.encodeIfPresent(download_count, forKey: .download_count)
		try container.encodeIfPresent(downloadUrl, forKey: .downloadUrl)
		try container.encodeIfPresent(categoryTitle, forKey: .categoryTitle)
		try container.encodeIfPresent(courseTitle, forKey: .courseTitle)
		try container.encodeIfPresent(courseTitle, forKey: .courseTitle)
		try container.encodeIfPresent(courseTitle, forKey: .courseTitle)
		try container.encodeIfPresent(_downloadProgress, forKey: .downloadProgress)
		try container.encodeIfPresent(_downloadStatus, forKey: .downloadStatus)
	}

	/// Decodes json into model
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
		file_name = try container.decodeIfPresent(String.self, forKey: .file_name) ?? ""
		content_type = try container.decodeIfPresent(String.self, forKey: .content_type) ?? ""
		download_count = try container.decodeIfPresent(Int.self, forKey: .download_count) ?? 0

		let downloadString = try container.decodeIfPresent(String.self, forKey: .downloadUrl) ?? ""
		downloadUrl = URL(string: downloadString)

		categoryTitle = try container.decodeIfPresent(String.self, forKey: .categoryTitle)
		courseTitle = try container.decodeIfPresent(String.self, forKey: .courseTitle)

		_downloadProgress = try container.decodeIfPresent(Double.self, forKey: .downloadProgress)
		_downloadStatus = try container.decodeIfPresent(Int.self, forKey: .downloadStatus)
	}

}

extension CourseFile {
	var downloadStatus: FileDownloadStatus {
		set {
			let status = newValue.toInt()
			self._downloadStatus = status.0
			self._downloadProgress = status.1
		}get {
			return FileDownloadStatus.fromInt(status: _downloadStatus ?? 0, progress: _downloadProgress ?? 0)
		}
	}

	var fileExtension: FileExtensions {
		return FileExtensions.fromString(fileName: file_name)
	}
	var fileNameWithoutExtension: String {
		return (file_name as NSString).deletingPathExtension
	}

	func localLocation() -> URL {
		let courseDirectory = TechnoDevDirectories.userDirectory
			.appendingPathComponent("\(self.categoryTitle ?? "" )")
			.appendingPathComponent("\(self.courseTitle ?? "" )")

		let mediaDirectory = courseDirectory.mediaDirectory(fileExtension: self.fileExtension)
		let fileDirectory = mediaDirectory.appendingPathComponent("\(self.fileNameWithoutExtension)")
			.appendingPathExtension("\(self.fileExtension.rawValue)")
		return fileDirectory
	}
}

//MARK: Preview
extension CourseFile {

	static func showFilePreviewScene(navigationController: CCNNavigationController, filesList: [URL], selectedPosition: Int) {
		var scene: NSViewController?
		let file = filesList[selectedPosition]
		let fileExtension = FileExtensions.fromString(fileName: file.lastPathComponent)
		switch fileExtension {
		case .mp3,.mp4:
			let mediaScene = MediaPlayerViewController.instantiate()
			mediaScene.files = filesList
			mediaScene.selectedFilePosition = selectedPosition
			scene = mediaScene
		case .pdf:
			let pdfViewer = PdfReaderViewController()
			pdfViewer.fileUrl = filesList[selectedPosition]
			scene = pdfViewer
		case .epub:
			let escene = EpubReaderViewController.instantiate()
			escene.fileUrl = filesList[selectedPosition]
			scene = escene
		case .unknown:
			break
		}

		if let newScene = scene {
			navigationController.pushViewController(newScene, animated: true)
			(navigationController as? TechnoDevNavigationController)?.addBreadCrum(breadCrum: BreadCrumItem(title: file.lastPathComponent))
		}else {
			let alert = NSAlert.newAlert(message: "Unsupported File", info: "\(fileExtension.rawValue) format is not supported by the application", style: .informational)
			alert.runModal()
		}
	}
/*
	private static func epubScene (book: EPUBDocument) -> EpubReaderViewController {
		var chaps = [EPUBManifestItem]()
		for spineItem in book.spine.items {
			if let item = book.manifest.items[spineItem.idref] {
				chaps.append(item)
			}
		}
		var indexes = [String]()
		//			indexes.append("\(-1)")
		for index in 0 ..< chaps.count {
			indexes.append("\(index)")
		}

		let epubReader = EpubReaderViewController()
		epubReader.book = book
		epubReader.chapters = chaps
		epubReader.indexes = indexes

		return epubReader
	}*/
}
