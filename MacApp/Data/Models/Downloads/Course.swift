//
//  Course.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Result
import Moya


struct CoursesData: Decodable {

	var downloadAllUrl: URL?
	var coursesCount = 0
	var courses: [Course] = []

	enum CodingKeys: String,CodingKey {
		case downloadAllUrl = "download_all"
		case coursesCount = "total_courses"
		case courses = "courses"
	}

	/// Decodes json into model
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		let downloadAllString = try container.decodeIfPresent(String.self, forKey: .downloadAllUrl) ?? ""
		downloadAllUrl = URL(string: downloadAllString)

		coursesCount = try container.decodeIfPresent(Int.self, forKey: .coursesCount) ?? 0
		courses = try container.decodeIfPresent([Course].self, forKey: .courses) ?? []
	}
}

struct Course: Decodable {
	var id: Int = 0
	var title: String = ""
	var category: String = ""
	var imageUrl: String = ""
	var availableFilesId = [Int]()

	///temporary property used for disabling download button in course list
	var isEnableDownload = true

	///available for /download_course api only
	var filesCount: Int = 0

	///available for /download_course api only
	var files = [CourseFile]()

	init() { }

	func thumbnailUrl(size: Int) -> URL? {
		let path = imageUrl.replacingOccurrences(of: "widthxheight", with: "\(size)")
		return URL(string: path)
	}

	enum CodingKeys: String,CodingKey {
		case id
		case title
		case category
		case imageUrl = "image_url"
		case availableFilesId = "available_course_files_ids"
		case filesCount = "course_files_count"
		case files = "course_files"
	}

	/// Decodes json into model
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)

		id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
		title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
		category = try container.decodeIfPresent(String.self, forKey: .category) ?? ""
		imageUrl = try container.decodeIfPresent(String.self, forKey: .imageUrl) ?? ""
		availableFilesId = try container.decodeIfPresent([Int].self, forKey: .availableFilesId) ?? []
		filesCount = try container.decodeIfPresent(Int.self, forKey: .filesCount) ?? 0
		files = try container.decodeIfPresent([CourseFile].self, forKey: .files) ?? []
	}
}

