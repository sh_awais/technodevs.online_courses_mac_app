//
//  CourseData.swift
//  MacApp
//
//  Created by Mansoor Ali on 17/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import Result
/*
class CourseData {
	private static let keyCourses = "KeyCourses"

	let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)

	static let shared = CourseData()

	/*
	private var _coursesList = PurchasedCourses()
	var coursesList: PurchasedCourses {
		return _coursesList
	}
	*/

	private var _courseCategories = [CourseCategory]()
	var courseCategories: [CourseCategory] {
		return _courseCategories
	}

	private init() {
//		_coursesList = read()
		_courseCategories = read()
	}

	//CRUD
	/*
	func set(newList: PurchasedCourses)  {
		_coursesList = newList
	}

	func save() {
		let data = _coursesList.jsonEncodedData
		queue.sync {
			UserDefaults.standard.set(data, forKey: CourseData.keyCourses)
		}
	}

	private func read() -> PurchasedCourses {
			guard let data = UserDefaults.standard.data(forKey: CourseData.keyCourses) else {
				return PurchasedCourses()
			}

			let coursesList: PurchasedCourses
			do {
				coursesList = try JSONDecoder().decode(PurchasedCourses.self, from: data)
			}catch {
				coursesList = PurchasedCourses()
			}
			return coursesList
	}

	func delete()  {
		_coursesList = PurchasedCourses()
		UserDefaults.standard.removeObject(forKey: CourseData.keyCourses)
		UserDefaults.standard.synchronize()
	}

	func fetchCourses(completion: @escaping(Result<PurchasedCourses,ResponseMessage>) ->Void)  {
		if !_coursesList.courses.isEmpty {
			completion(Result.success(_coursesList))
		}else {
			PurchasedCourses.fetchCourses(token: LoginSession.current.token, completion: completion)
		}
	}

// Update Course File

func updateFile(file: CourseFile)  {
for (courseIndex,course) in coursesList.courses.enumerated() {
if course.title.caseInsensitiveCompare(file.courseTitle ?? "") == .orderedSame  {
for (index, oldFile) in course.coure_files.enumerated() {
if oldFile.id == file.id {
_coursesList.courses[courseIndex].coure_files[index] = file
}
}
}
}

//TODO: if save on terminate donot work save on file update
//		save()
}

//Methods for accesing data
private func isCourseAvailable(atIndex index: Int) -> Bool {
guard index < _coursesList.courses.count else {
return false
}
return true
}*/

	//Course Categories CRUD
	func set(courseCategories: [CourseCategory])  {
		_courseCategories = courseCategories
	}

	func save() {
		let data = _courseCategories.jsonEncodedData
		queue.sync {
			UserDefaults.standard.set(data, forKey: CourseData.keyCourses)
		}
	}

	private func read() -> [CourseCategory] {
		guard let data = UserDefaults.standard.data(forKey: CourseData.keyCourses) else {
			return []
		}

		let courseCategories: [CourseCategory]
		do {
			courseCategories = try JSONDecoder().decode([CourseCategory].self, from: data)
		}catch {
			courseCategories = []
		}
		return courseCategories
	}

	func delete()  {
		_courseCategories = []
		UserDefaults.standard.removeObject(forKey: CourseData.keyCourses)
		UserDefaults.standard.synchronize()
	}

	//API
	func fetchCourseCategories(completion: @escaping(Result<[CourseCategory],ResponseMessage>) ->Void)  {
		if !_courseCategories.isEmpty {
			completion(Result.success(_courseCategories))
		}else {
//			PurchasedCourses.fetchCourses(token: LoginSession.current.token) { [weak self] (result) in
//				switch result {
//				case .success(let purchasedCourses):
//					let courseCategories = self?.getCoursesCategories(courses: purchasedCourses.courses) ?? []
//					completion(Result.success(courseCategories))
//				case .failure(let error):
//					completion(Result.failure(error))
//				}
//			}
		}
	}

	//create categories and put courses into these categories
	private func getCoursesCategories(courses: [Course]) -> [CourseCategory] {
		var categoriesDict = [String: CourseCategory]()
		for course in courses {
			let categoryTitle = course.category
			if categoriesDict[categoryTitle] != nil {
				categoriesDict[categoryTitle]?.courses.append(course)
			}else {
				categoriesDict[categoryTitle] = CourseCategory(title: categoryTitle, courses: [course])
			}
		}
		let categoriesSorted = categoriesDict.values.sorted(by: {$0.title <  $1.title})
		return categoriesSorted
	}

	func category(atIndex index: Int) -> CourseCategory {
		guard index < self._courseCategories.count else {
			return CourseCategory(title: "Empty Category", courses: [])
		}
		return self._courseCategories[index]
	}

	func coursesList(forCategoryAtIndex categoryIndex: Int) -> [Course] {
		return category(atIndex: categoryIndex).courses
	}

	func course(forCategoryAtIndex categoryIndex: Int, courseIndex index: Int) -> Course {
		let courseList = coursesList(forCategoryAtIndex: categoryIndex)
		guard index < courseList.count else {
			return Course()
		}
		return courseList[index]
	}

	func courseFilesList(forCategoryAtIndex categoryIndex: Int, courseIndex index: Int) -> Course {
		return course(forCategoryAtIndex: categoryIndex, courseIndex: index)
	}

	func courseFile(forCategoryAtIndex categoryIndex: Int,courseIndex: Int, fileIndex: Int) -> CourseFile {
		let course = courseFilesList(forCategoryAtIndex: categoryIndex, courseIndex: courseIndex)
		guard fileIndex < course.course_files.count else {
			return CourseFile()
		}
		return course.course_files[fileIndex]
	}

	// Update Course File

	func updateFile(file: CourseFile)  {
		for category in _courseCategories {
			for (courseIndex,course) in category.courses.enumerated() {
				if course.title.caseInsensitiveCompare(file.courseTitle ?? "") == .orderedSame  {
					for (index, oldFile) in course.course_files.enumerated() {
						if oldFile.id == file.id {
							category.courses[courseIndex].course_files[index] = file
						}
					}
				}
			}
		}

		//TODO: if save on terminate donot work save on file update
		//		save()
	}

	//Methods for accesing data
	/*
	private func isCourseAvailable(atIndex index: Int) -> Bool {
		guard index < _courseCategories.courses.count else {
			return false
		}
		return true
	}
	*/

}
*/
