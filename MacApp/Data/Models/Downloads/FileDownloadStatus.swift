//
//  FileDownloadStatus.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

enum FileDownloadStatus: Equatable {

	case notStarted
	case downloading(Double)
	case paused(Double)
	case failed
	case downloaded

	var progress: Double {
		switch self {
		case .downloading(let progress):
			return progress
		case .paused(let progress):
			return progress
		default:
			return 0
		}
	}

	func toInt() -> (Int, Double) {
		switch self {
		case .notStarted:
			return (0,0)
		case .downloading(let progress):
			return (1,progress)
		case .paused(let progress):
			return (2,progress)
		case .failed:
			return (3,0)
		case .downloaded:
			return (4,0)
		}
	}

	static func fromInt(status: Int, progress: Double) -> FileDownloadStatus {
		switch status {
		case 0:
			return .notStarted
		case 1:
			return .downloading(progress)
		case 2:
			return .paused(progress)
		case 3:
			return .failed
		case 4:
			return .downloaded
		default:
			return .notStarted
		}
	}

	public static func == (lhs: FileDownloadStatus, rhs: FileDownloadStatus) -> Bool {
		switch (lhs,rhs) {
		case (.downloading(_),.downloading(_)):
			return true
		case (.downloaded,.downloaded):
			return true
		case (.paused(_),.paused(_)):
			return true
		case (.failed,.failed):
			return true
		default:
			return false
		}
	}
}
