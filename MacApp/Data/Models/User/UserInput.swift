//
//  UserInput.swift
//  MacApp
//
//  Created by Mansoor Ali on 24/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

class UserInput: Encodable {
	let authToken: String
	let currentPassword: String
	var firstName: String?
	var lastName: String?
	var company: String?
	var password: String?

	enum CodingKeys: String, CodingKey {
		case authToken = "auth_token"
		case currentPassword = "user[current_password]"
		case firstName = "user[first_name]"
		case lastName = "user[last_name]"
		case company = "user[company]"
		case password = "user[password]"
	}

	init(authToken: String, currentPassword: String) {
		self.authToken = authToken
		self.currentPassword = currentPassword
	}
}
