//
//  SessionToken.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

struct SessionToken: Encodable {
	let auth_token: String
}
