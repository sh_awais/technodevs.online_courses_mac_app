//
//  LoginInput.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

struct LoginInput: Encodable {
	let email: String
	let password: String

	init(email: String, password: String) {
		self.email = email.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
		self.password = password.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
	}
}
