//
//  LoginResponse.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

///1.	Maintains a single login session for user
///2.	ensure only one session is stores at a time
///3.	save and read session info from user defaults
class LoginSession {
	private var loginResponse: LoginResponse?
	static let current = LoginSession()

	var isSessionExist: Bool {
		if loginResponse == nil {
			loadStoredSession()
		}
		return loginResponse != nil
	}

	//Session Token
	var token: SessionToken {
		return SessionToken(auth_token: loginResponse?.auth_token ?? "")
	}

	//User info
	var userName: String {
		return "\(firstName) \(lastName)"
	}
	var firstName: String { return loginResponse?.first_name ?? ""}
	var lastName: String { return loginResponse?.last_name ?? ""}
	var email: String { return loginResponse?.email ?? "" }
	var company: String { return loginResponse?.company ?? "" }

	//Read session from UserDefaults
	private func loadStoredSession()  {
		self.loginResponse = LoginResponse.read()
	}

	//set new login response
	func set(loginResponse: LoginResponse)  {
		loginResponse.save()
		self.loginResponse = loginResponse
	}

	private func deleteSession()  {
		LoginResponse.delete()
		loginResponse = nil
	}

	func logout()  {
//		deleteSession()
		//clear download manager before removing session info
		//because it uses user email to create keyX
		deleteSession()
		DownloadManager.shared.delete()
//		CourseData.shared.delete()
		AppDelegate.shared.showLoginScene()
	}
}

//Response when user is successfully logged In
class LoginResponse: Codable {

	var auth_token = ""
	var email = ""
	var first_name = ""
	var last_name = ""
	var company = ""
	
	static private let keyLoginResponse = "keyLoginResponse"

	fileprivate func save() {
		UserDefaults.standard.set(self.jsonEncodedData, forKey: LoginResponse.keyLoginResponse)
		UserDefaults.standard.synchronize()
	}

	fileprivate static func read() -> LoginResponse? {
		guard let data = UserDefaults.standard.data(forKey: keyLoginResponse) else {
			return nil
		}
		return try? JSONDecoder().decode(LoginResponse.self, from: data)
	}

	fileprivate static func delete() {
		UserDefaults.standard.removeObject(forKey: keyLoginResponse)
		UserDefaults.standard.synchronize()
	}
}
