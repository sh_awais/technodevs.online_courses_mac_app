//
//  TechnoDevController.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class TechnoDevController: NSViewController {

	override func viewDidLoad() {
		super.viewDidLoad()

		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.white.cgColor
		
	}

}
