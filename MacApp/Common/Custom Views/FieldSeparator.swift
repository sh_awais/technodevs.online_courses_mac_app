//
//  FieldSeparator.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class FieldSeparator: NSView {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }

	override func awakeFromNib() {
		super.awakeFromNib()

		self.wantsLayer = true
		self.layer?.backgroundColor = R.color.secondaryTextDark.cgColor
	}
    
}
