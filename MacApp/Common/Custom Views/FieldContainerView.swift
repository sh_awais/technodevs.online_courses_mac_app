//
//  FieldContainerView.swift
//  MacApp
//
//  Created by Mansoor Ali on 20/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class FieldContainerView: NSView {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.wantsLayer = true
		self.layer?.borderColor = R.color.separator.cgColor
		self.layer?.borderWidth = 1
		self.layer?.cornerRadius = 5
	}
    
}
