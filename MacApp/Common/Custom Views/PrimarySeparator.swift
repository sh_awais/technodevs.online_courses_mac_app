//
//  FieldSeparator.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class PrimarySeparator: NSView {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.wantsLayer = true
		self.layer?.backgroundColor = R.color.separator.cgColor
	}
    
}
