//
//  CardView.swift
//  MacApp
//
//  Created by Mansoor Ali on 08/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class CardView: NSView {
	
	@IBInspectable var shadowOffsetX: CGFloat = 0
	@IBInspectable var shadowOffsetY: CGFloat = 0
	@IBInspectable var cornerRadius: CGFloat = 4.0
	@IBInspectable var shadoRadius: CGFloat = 1
	@IBInspectable var shadoOpacity: Float = 0.5

	override func awakeFromNib() {
		super.awakeFromNib()

		let shadow = NSShadow()
		shadow.shadowColor = NSColor.black.withAlphaComponent(0.5)
		shadow.shadowOffset = CGSize(width: shadowOffsetX, height: shadowOffsetY)
		shadow.shadowBlurRadius = shadoRadius
		self.shadow = shadow
		self.wantsLayer = true
		layer?.cornerRadius = cornerRadius
		layer?.shadowOpacity = shadoOpacity
		layer?.backgroundColor = NSColor.white.cgColor
	}
}
