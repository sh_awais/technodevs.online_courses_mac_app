//
//  HeaderView.swift
//  MacApp
//
//  Created by Mansoor Ali on 08/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class HeaderView: NSView {

	let title: NSTextField = {
		let lbl = HeadlineLabel(wrappingLabelWithString: "Your Heder goes here")
		lbl.isBordered = false
		lbl.isEditable = false
		lbl.isSelectable = false
		return lbl
	}()

	private let bottomBorder: NSView = {
		let view = NSView()
		view.wantsLayer = true
		view.layer?.backgroundColor = R.color.separator.cgColor
		return view
	}()

	private let higlightView: NSView = {
		let view = NSView()
		view.wantsLayer = true
		view.layer?.backgroundColor = R.color.primary.cgColor
		return view
	}()


	private var highlightViewHeight: NSLayoutConstraint?

	override func awakeFromNib() {
		super.awakeFromNib()

		customizeUI()
	}

	private func customizeUI() {

		add(label: title, to: self)

		add(bottomBorder: bottomBorder, to: self)

		add(higlightBorder: higlightView, to: self, anchorView: title)
	}

	private func add(label child: NSTextField, to parent: NSView) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			child.topAnchor.constraint(equalTo: parent.topAnchor),
			child.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
			parent.trailingAnchor.constraint(greaterThanOrEqualTo: child.trailingAnchor, constant: 0)
		])
	}

	private func add(bottomBorder child: NSView, to parent: NSView) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			child.leadingAnchor.constraint(equalTo: parent.leadingAnchor),
			parent.trailingAnchor.constraint(equalTo: child.trailingAnchor),
			parent.bottomAnchor.constraint(equalTo: parent.bottomAnchor),
			child.heightAnchor.constraint(equalToConstant: 1)
		])
	}

	private func add(higlightBorder child: NSView, to parent: NSView, anchorView: NSTextField) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			child.leadingAnchor.constraint(equalTo: anchorView.leadingAnchor),
			anchorView.trailingAnchor.constraint(equalTo: child.trailingAnchor),
			parent.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
		])

		highlightViewHeight = child.heightAnchor.constraint(equalToConstant: 1)
		highlightViewHeight?.isActive = true
	}

	//If you want to change font of default label
	func set(font: NSFont, color: NSColor = R.color.primaryTextDark)  {
		self.title.font = font
	}

	func set(highlightViewHeight: CGFloat)  {
		self.highlightViewHeight?.constant = highlightViewHeight
	}
    
}
