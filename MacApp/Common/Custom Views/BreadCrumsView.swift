//
//  HeaderView.swift
//  MacApp
//
//  Created by Mansoor Ali on 08/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

struct BreadCrumItem {
	let title: String
}

protocol BreadCrumsViewDelegate:class {
	func breadCrumsView(popTo position: Int)
}

class BreadCrumsView: NSView {

	//only shows when items array is empty
	private let firstPageTitle: NSTextField = {
		let lbl = NSTextField(wrappingLabelWithString: "")
		lbl.isBordered = false
		lbl.isEditable = false
		lbl.isSelectable = false
		lbl.font = R.font.robotoRegular(size: 12)
		lbl.textColor = R.color.primaryTextDark
		return lbl
	}()

	//if there is only one item in array this is displayed
	var firstPageTitleString: NSAttributedString? = NSAttributedString(string: "Your title goes here") {
		didSet {
			firstPageTitle.attributedStringValue = firstPageTitleString ?? NSAttributedString(string: "")
			if firstPageTitleString != nil && buttons.count == 1{
				showTitle()
			}else {
				hideTitle()
			}
		}
	}
	weak var delegate: BreadCrumsViewDelegate?

	private var buttons = [NSButton]()
	private var separators = [NSTextField]()

	private var highlightViewHeight: NSLayoutConstraint?

	override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		customizeUI()
	}

	required init?(coder decoder: NSCoder) {
		super.init(coder: decoder)
		customizeUI()
	}

	override func awakeFromNib() {
		super.awakeFromNib()

		customizeUI()
	}

	private func customizeUI() {
		add(label: firstPageTitle, to: self)
	}

	private func add(label child: NSTextField, to parent: NSView) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			parent.bottomAnchor.constraint(equalTo: child.bottomAnchor),
			child.leadingAnchor.constraint(equalTo: parent.leadingAnchor)
		])
	}

	private func add(button child: NSButton, to parent: NSView, anchorView: NSView, isFirstButton: Bool) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			parent.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
		])

		if isFirstButton {
			child.leadingAnchor.constraint(equalTo: anchorView.leadingAnchor).isActive = true
		}else {

			child.leadingAnchor.constraint(equalTo: anchorView.trailingAnchor).isActive = true
		}
	}

	private func add(separator child: NSTextField, to parent: NSView, anchorView: NSView) {
		child.translatesAutoresizingMaskIntoConstraints = false
		parent.addSubview(child)

		NSLayoutConstraint.activate([
			parent.bottomAnchor.constraint(equalTo: anchorView.bottomAnchor),
			child.leadingAnchor.constraint(equalTo: anchorView.trailingAnchor),
			child.widthAnchor.constraint(equalToConstant: 14)
			])
	}

	//public interface
	func insert(newItem: BreadCrumItem)  {

		//add button to array
		let button = NSButton(frame: .zero)
		buttons.append(button)

		//configure
		let tag = buttons.count-1
		configureNewButton(newButton: button, tag: tag, breadCrumItem: newItem)

		//get anchorView
		if buttons.count == 1 {
			add(button: button, to: self, anchorView: self, isFirstButton: true)
		}else {
			let newSeparator = addNewSeparator(anchorView: buttons[buttons.count-2])
			add(button: button, to: self, anchorView: newSeparator, isFirstButton: false)
		}

		if firstPageTitleString != nil && buttons.count == 1{
			showTitle()
		}else {
			hideTitle()
		}

		updatePreviousButtonColor()
	}

	//configure button action, content and ui
	private func configureNewButton(newButton: NSButton, tag: Int, breadCrumItem: BreadCrumItem) {
		//add new button
		newButton.isBordered = false
		newButton.title = breadCrumItem.title
		newButton.attributedTitle = newButton.titleWithColor(color: R.color.primary)

		//set tag
		newButton.tag = tag

		//action
		newButton.target = self
		newButton.action = #selector(actNavigation(_:))
	}

	//add new separator after anchorView
	private func addNewSeparator(anchorView: NSView) -> NSView {
		let separator = NSTextField(wrappingLabelWithString: "/")
		separator.isBordered = false
		separator.isEditable = false
		separator.isSelectable = false
		separator.alignment = .center

		separators.append(separator)

		add(separator: separator, to: self, anchorView: anchorView)

		return separator
	}

	//Update previouds button color to dark
	private func updatePreviousButtonColor() {
		let previous = buttons.count - 2
		if previous > -1  {
			buttons[previous].attributedTitle = buttons[previous].titleWithColor(color: R.color.primaryTextDark)
		}
	}

	private func showTitle() {
		buttons.first?.isHidden = true
		firstPageTitle.isHidden = false
		firstPageTitle.attributedStringValue = firstPageTitleString ?? NSAttributedString(string: "")
	}

	private func hideTitle() {
		firstPageTitle.isHidden = true
		buttons.first?.isHidden = false
	}

	private func removeButtonsAfter(tag: Int) {
		let startIndex = tag+1
		let buttonsCopy = buttons
		for i in startIndex..<buttonsCopy.count {
			let button = buttons[i]
			button.removeFromSuperview()
		}
		buttons = Array(buttons[0..<startIndex])
	}

	private func removeSeparatorsAfter(tag: Int) {
		let separatorsCopy = separators
		for i in tag..<separatorsCopy.count {
			let button = separators[i]
			button.removeFromSuperview()
		}
		separators = Array(separators[0..<tag])
	}

	//MARK: Action
	@objc private func actNavigation(_ sender: NSButton) {
		let tag = sender.tag
		guard tag < buttons.count - 1 else {
			return
		}
		removeButtonsAfter(tag: tag)
		removeSeparatorsAfter(tag: tag)
		buttons[tag].attributedTitle = buttons[tag].titleWithColor(color: R.color.primary)

		if firstPageTitleString != nil && buttons.count == 1{
			showTitle()
		}else {
			hideTitle()
		}
		delegate?.breadCrumsView(popTo: tag)
	}

	func popLastItem() {
		let itemPositions = buttons.count - 1
		let secondLastItem = itemPositions - 1
		if secondLastItem < buttons.count {
			actNavigation(buttons[secondLastItem])
		}
	}
}
