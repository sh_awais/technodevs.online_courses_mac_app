//
//  Notification.swift
//  MacApp
//
//  Created by Mansoor Ali on 14/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

extension NSNotification.Name{
	enum TechnoDevNotifications: String {
		case fileProgressUpdated = "com.Technodev.fileProgressUpdated"
		case fileDownloadCompleted = "com.Technodev.fileDownloadCompleted"
		case fileDownloadingPaused = "com.Technodev.fileDownloadingPaused"
		case fileDownloadingCanceled = "com.Technodev.fileDownloadingCanceled"
		case fileDownloadingFailed = "com.Technodev.fileDownloadingFailed"
		case hideSideMenu = "com.Technodev.hideSideMenu"
		case showSideMenu = "com.Technodev.showSideMenu"
	}

	init(_ value: TechnoDevNotifications) {
		self = NSNotification.Name(value.rawValue)
	}
}
