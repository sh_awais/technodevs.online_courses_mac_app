//
//  TechnoDevController.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import CCNNavigationController

class TechnoDevNavigationController: CCNNavigationController {

	private let breadCrumsView: BreadCrumsView = {
		return BreadCrumsView(frame: .zero)
	}()

	var firstPageTitle: NSAttributedString?
	private var breadCrumsTop: NSLayoutConstraint?

	override func viewDidAppear() {
		super.viewDidAppear()

		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.white.cgColor

		breadCrumsView.delegate = self
		breadCrumsView.firstPageTitleString = firstPageTitle
		add(breadCrums: breadCrumsView, to: self.view)
		breadCrumsView.frame = breadCrumsFrame()

	}

	override func viewDidLayout() {
		super.viewDidLayout()
		breadCrumsView.frame = breadCrumsFrame()
		print(breadCrumsFrame().origin.y)
	}

	private func breadCrumsFrame() -> NSRect {
		return CGRect(x: 20, y: self.view.frame.size.height - 40, width: self.view.frame.width-40, height: 20)
	}

	private func add(breadCrums child: BreadCrumsView, to parent: NSView) {
		parent.addSubview(child)
	}

	func addBreadCrum(breadCrum: BreadCrumItem) {
		self.breadCrumsView.insert(newItem: breadCrum)

		DispatchQueue.main.asyncAfter(deadline: .now()+0.1) { [weak self] in
			self?.view.bringSubView(toFront: self?.breadCrumsView)
		}
	}

	func removeLastBreadCrumItem() {
		self.breadCrumsView.popLastItem()
	}

	func toggeBreadCrumb(isHide: Bool) {
		self.breadCrumsView.isHidden = isHide
	}
}

//MARK: BreadCrumsViewDelegate
extension TechnoDevNavigationController: BreadCrumsViewDelegate {

	func breadCrumsView(popTo position: Int) {

		//check if index is out of range
		guard position < self.viewControllers.count else {
			return
		}

		//pop controller
		let controller = self.viewControllers[position]
		self.pop(to: controller, animated: true)

		self.view.bringSubView(toFront: self.breadCrumsView)
		DispatchQueue.main.asyncAfter(deadline: .now()+0.1) { [weak self] in
			self?.view.bringSubView(toFront: self?.breadCrumsView)
		}
	}
}
