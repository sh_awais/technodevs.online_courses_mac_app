//
//  TitleTextField.swift
//  MacApp
//
//  Created by Mansoor Ali on 12/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class PrimaryTextField: NSTextField {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.font = R.font.robotoRegular(size: 14)

	}
}

class SecureTextField: NSSecureTextField {
	override func awakeFromNib() {
		super.awakeFromNib()

		self.font = R.font.robotoRegular(size: 14)

	}
}

class BorderedTextField: NSSecureTextField {
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.font = R.font.robotoRegular(size: 14)
	}
}


