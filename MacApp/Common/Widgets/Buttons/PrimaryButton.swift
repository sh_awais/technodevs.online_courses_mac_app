//
//  PrimaryButton.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class PrimaryButton: BaseButton {

	override var textColor: NSColor {
		get {
			return R.color.primaryTextLight
		}
		set {

		}
	}

	override func customizeUI() {
		super.customizeUI()
		self.wantsLayer = true
		self.layer?.backgroundColor = R.color.primary.cgColor
		self.layer?.cornerRadius = 4
		font = R.font.robotoBold(size: 16)
	}

}
