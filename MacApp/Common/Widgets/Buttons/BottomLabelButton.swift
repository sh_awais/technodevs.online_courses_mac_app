//
//  BottomLabelButton.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class BottomLabelButton: BaseButton {

	override var textColor: NSColor {
		get {
			return R.color.primaryTextDark
		}
		set {

		}
	}

	override func customizeUI() {
		super.customizeUI()
		
		font = R.font.robotoBold(size: 14)
	}
    
}
