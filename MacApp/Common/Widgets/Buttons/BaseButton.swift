//
//  BaseButton.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class BaseButton: NSButton {

	var textColor: NSColor = R.color.primaryTextDark

	override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		customizeUI()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		customizeUI()
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		customizeUI()

	}

	func customizeUI() {
		setTitleColor()
	}

	private func setTitleColor() {

		self.attributedTitle = titleWithColor(color: textColor)
	}

	func runTapAnimation() {
		NSAnimationContext.runAnimationGroup({ [weak self] (context) in
			context.allowsImplicitAnimation = true
			context.duration = 0.1
			guard let self = self else { return }
			self.attributedTitle = self.titleWithColor(color: self.textColor.withAlphaComponent(0.5))
		}) { [weak self] in
			guard let self = self else { return }
			self.attributedTitle = self.titleWithColor(color: self.textColor)
		}
	}
    
}
