//
//  PrimaryButton.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class LinkPrimaryButton: BaseButton {

	override var textColor: NSColor {
		get {
			return R.color.primary
		}
		set { }
	}

	override func customizeUI() {
		super.customizeUI()

		addBottomBorder(color: R.color.primary)

		font = R.font.robotoBold(size: 14)
	}


}


class LinkSecondaryButton: BaseButton {

	override var textColor: NSColor {
		get {
			return R.color.primaryTextDark
		}
		set { }
	}

	override func customizeUI() {
		super.customizeUI()

		addBottomBorder(color: R.color.primaryTextDark)
	}
}
