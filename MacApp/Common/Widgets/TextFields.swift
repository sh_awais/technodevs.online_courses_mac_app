//
//  TitleTextField.swift
//  MacApp
//
//  Created by Mansoor Ali on 12/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class TitleTextField: NSTextField {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.font = NSFont.systemFont(ofSize: 14, weight: .medium)

	}
}

class SubheadTextField: NSTextField {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.font = NSFont.systemFont(ofSize: 13, weight: .regular)
		self.textColor = NSColor.lightGray
	}
}

class BodyTextField: NSTextField {

	override func awakeFromNib() {
		super.awakeFromNib()

		self.font = NSFont.systemFont(ofSize: 13, weight: .regular)
	}
}

