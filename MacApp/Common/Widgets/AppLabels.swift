//
//  AppLabels.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

class BaseLabel: NSTextField {

	@IBInspectable var isLight: Bool = false {
		didSet {
			updateTextColor(isLight: isLight)
		}
	}
	override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		customizeUI()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		customizeUI()
	}

	func customizeUI()  { }

	func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.primaryTextDark
		}
	}
    
}

class HeadlineLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 18)
		updateTextColor(isLight: isLight)
	}
}


class MenuLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 14)
		updateTextColor(isLight: isLight)
	}
}

class TitleLabelSecondary: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 14)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.secondaryTextDark
		}
	}
}

class SubHeadLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 15)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.secondaryTextDark
		}
	}
}

class CaptionLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoMedium(size: 15)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.secondaryTextDark
		}
	}
}

class Caption2Label: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoMedium(size: 18)
		updateTextColor(isLight: isLight)
	}
}


class HeaderLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoMedium(size: 16)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.secondaryTextDark
		}
	}
}

class HeaderLabelRegular: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 16)
		updateTextColor(isLight: isLight)
	}
}

class FootnoteLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 13)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.secondaryTextDark
		}
	}
}

class UserInfoLabel: BaseLabel {

	override func customizeUI() {
		self.font = R.font.robotoRegular(size: 15)
		updateTextColor(isLight: isLight)
	}

	override func updateTextColor(isLight: Bool)  {
		if isLight {
			textColor = R.color.primaryTextLight
		}else {
			textColor = R.color.textTertiaryDark
		}
	}
}

