//
//  CourseFileCollectionViewItem.swift
//  MacApp
//
//  Created by Mansoor Ali on 23/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

protocol CourseFileCollectionViewItemDelegate: class {
	func courseFileCollectionViewItem(fileDownloadedForCell cell: CourseFileCollectionViewItem, newFile: CourseFile)
	func courseFileCollectionViewItem(downloadFileForCell cell: CourseFileCollectionViewItem)
}

class CourseFileCollectionViewItem: NSCollectionViewItem, NibReusable {

	@IBOutlet weak private var icon: NSImageView!
	@IBOutlet weak private var slNo: NSTextField!
	@IBOutlet weak private var titleLbl: NSTextField!
	@IBOutlet weak private var subtitle: NSTextField!

	@IBOutlet weak private var downloadOptions: NSView!

	@IBOutlet weak private var btnDownload: NSButton!

	@IBOutlet weak private var progressContainer: NSView!
	@IBOutlet weak private var progressLabel: NSTextField!
	@IBOutlet weak private var progressIndicator: NSProgressIndicator!


	weak var delegate: CourseFileCollectionViewItemDelegate?

//	var downloadCallback: ((NSCollectionViewItem) -> Void)?
//	var openCallback: ((CourseFile) -> Void)?

	private var file: CourseFile?

	enum Style {
		case fileInfo(Int, URL, NSImage?)
		case download(Int, CourseFile)
	}

	override func awakeFromNib() {
		super.awakeFromNib()
		progressLabel.font = R.font.robotoRegular(size: 12)
		progressLabel.textColor = R.color.secondaryTextDark

		progressIndicator.controlSize = NSControl.ControlSize.regular
	}

	func configure(style: Style)  {
		switch style {
		case .fileInfo(let slno, let file, let icon):
			configureFileInfo(slNo: slno, file: file, icon: icon)
		case .download(let slno, let file):
			configureDownload(slNo: slno, file: file)
		}
	}

	private func configureFileInfo(slNo: Int, file: URL, icon: NSImage?) {
		
		self.slNo.stringValue = String(format: "%02d.", slNo)
		self.titleLbl.stringValue = file.lastPathComponent
		self.icon.image = icon?.tintedImage(tint: R.color.primary)

		self.icon.isHidden = false
		self.subtitle.isHidden = true
		self.downloadOptions.isHidden = true
	}

	private func configureDownload(slNo: Int, file: CourseFile) {

		self.file = file

		self.slNo.stringValue = String(format: "%02d.", slNo+1)
		self.titleLbl.stringValue = file.file_name

		self.icon.isHidden = true
		self.subtitle.isHidden = true
		self.downloadOptions.isHidden = false

		btnDownload.image = R.image.downloads.tintedImage(tint: R.color.secondary)
		btnDownload.font = R.font.robotoBold(size: 12)

		configure(status: file.downloadStatus)
	}

	private func confirmationAlert(completion:@escaping(Bool) -> Void) {
		let alert = NSAlert.newAlert(message: "File already exists", info: "If you want to redownload file please confirm your request", style: .informational)
		alert.addButton(withTitle: "Confirm")
		alert.addButton(withTitle: "Cancel")
		let response = alert.runModal()
		if response == NSApplication.ModalResponse.alertFirstButtonReturn {
			completion(true)
		}else {
			completion(false)
		}
	}

	//MARK: Action
	@IBAction func actDownload(_ sender: NSButton)  {
		if file?.localLocation().isFileExists ?? false {
			confirmationAlert { [weak self] (isConfirmed) in
				guard let self = self else { return }
				if isConfirmed {
					self.delegate?.courseFileCollectionViewItem(downloadFileForCell: self)
				}
			}
		}else {
			delegate?.courseFileCollectionViewItem(downloadFileForCell: self)
		}
//		delegate?.courseFileCollectionViewItem(downloadFileForCell: self)
	}

	func configure(status: FileDownloadStatus)  {
		switch status {
		case .notStarted,.downloaded, .failed:
			btnDownload.isHidden = false
			btnDownload.isEnabled = true
			progressContainer.isHidden = true
		case .downloading(let progress), .paused(let progress):
			btnDownload.isHidden = true
			progressContainer.isHidden = false
			progressIndicator.doubleValue = progress
			progressLabel.stringValue = "\(Int(progress))%"
		}
	}


	//MARK: Notifications
	func addNotificationObservers()  {
		NotificationCenter.default.addObserver(self, selector: #selector(progressUpdated(_:)), name: NSNotification.Name.init(.fileProgressUpdated), object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(fileDownloaded(_:)), name: NSNotification.Name.init(.fileDownloadCompleted), object: nil)
	}

	@objc private func progressUpdated(_ notification: NSNotification) {
		guard let newFile = notification.object as? CourseFile, let existingFileId = self.file?.id else {
			return
		}

		if newFile.id == existingFileId {
			configure(status: newFile.downloadStatus)
		}
	}

	@objc private func fileDownloaded(_ notification: NSNotification) {
		guard let newFile = notification.object as? CourseFile, let existingFileId = self.file?.id else {
			return
		}

		if newFile.id == existingFileId {
			self.file = newFile
			configure(status: newFile.downloadStatus)
			delegate?.courseFileCollectionViewItem(fileDownloadedForCell: self, newFile: newFile)
		}
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		addNotificationObservers()
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}
}
