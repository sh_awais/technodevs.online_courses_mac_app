//
//  AppDelegate.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa
import Swifter

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

	static let shared: AppDelegate = NSApplication.shared.delegate as! AppDelegate

	//setup a local server
	let server = HttpServer()
	let serverQueue = DispatchQueue.global(qos: .utility)

	func applicationDidFinishLaunching(_ aNotification: Notification) {
		// Insert code here to initialize your application


		UserDefaults.standard.set(true, forKey: "NSConstraintBasedLayoutVisualizeMutuallyExclusiveConstraint‌")
		
//		NSApplication.shared.windows.first?.centerAlign()

		//If session already exists take user to home
		if LoginSession.current.isSessionExist {
			showHomeScene()
		}else {
			showLoginScene()
		}

		DownloadManager.shared.loadHistory()
	}

	func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
		if let window = sender.windows.first {
			if flag {
				window.orderFront(nil)
			} else {
				window.makeKeyAndOrderFront(nil)
			}
		}
		return true
	}

	private func showHomeScene() {
		DispatchQueue.main.asyncAfter(deadline: .now()+0) {
			let homeScene = HomeViewController.instantiate()
			let window = NSApplication.shared.windows.first
			window?.contentViewController = homeScene
			window?.makeKey()
		}
	}

	func showLoginScene() {
		let homeScene = LoginViewController.instantiate()
		let window = NSApplication.shared.windows.first
		window?.contentViewController = homeScene
		window?.makeKey()
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application

//		CourseData.shared.save()
		DownloadManager.shared.save()
		server.stop()
	}

}

