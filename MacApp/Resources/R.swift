//
//  R.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

struct R {

	struct storyboard {
		static let main = NSStoryboard(name: "Main", bundle: nil)
		static let home = NSStoryboard(name: "Home", bundle: nil)
		static let downloads = NSStoryboard(name: "Downloads", bundle: nil)
		static let user = NSStoryboard(name: "User", bundle: nil)
		static let nonPurchasedCourses = NSStoryboard(name: "NonPurchasedCourses", bundle: nil)
		static let wifiTransfer = NSStoryboard(name: "WifiTransfer", bundle: nil)
		static let preview = NSStoryboard(name: "Preview", bundle: nil)
	}

	struct image {
		//side menu
		static var menu: NSImage { return NSImage(named: "menu") ?? NSImage() }
		static var profile: NSImage { return NSImage(named: "profile") ?? NSImage() }
		static var home: NSImage { return NSImage(named: "home") ?? NSImage() }
		static var downloads: NSImage { return NSImage(named: "downloads") ?? NSImage() }
		static var wifitransfer: NSImage { return NSImage(named: "wifiTransfer") ?? NSImage() }
		static var userInfo: NSImage { return NSImage(named: "userInfo") ?? NSImage() }
		static var courses: NSImage { return NSImage(named: "courses") ?? NSImage() }
		static var faqs: NSImage { return NSImage(named: "faqs") ?? NSImage() }
		static var menuSelection: NSImage { return NSImage(named: "menuSelection") ?? NSImage() }
		static var menuSelectionRound: NSImage { return NSImage(named: "menuSelectionRound") ?? NSImage() }
		static var logout: NSImage { return NSImage(named: "logout") ?? NSImage() }

		//home
		static var arrowLeft: NSImage { return NSImage(named: "arrowLeft") ?? NSImage() }
		static var arrowRight: NSImage { return NSImage(named: "arrowRight") ?? NSImage() }
		static var audio: NSImage { return NSImage(named: "audio") ?? NSImage() }
		static var video: NSImage { return NSImage(named: "video") ?? NSImage() }
		static var ebook: NSImage { return NSImage(named: "ebook") ?? NSImage() }

		//downloads
		static var play: NSImage { return NSImage(named: "play") ?? NSImage() }
		static var pause: NSImage { return NSImage(named: "pause") ?? NSImage() }
		static var retry: NSImage { return NSImage(named: "retry") ?? NSImage() }
		static var cross: NSImage { return NSImage(named: "cross") ?? NSImage() }
		static var videoRounded: NSImage { return NSImage(named: "videoRounded") ?? NSImage() }
		static var audioRounded: NSImage { return NSImage(named: "audioRounded") ?? NSImage() }
		static var ebookRounded: NSImage { return NSImage(named: "ebookRounded") ?? NSImage() }

		//user
		static var edit: NSImage { return NSImage(named: "edit") ?? NSImage() }
		static var lock: NSImage { return NSImage(named: "lock") ?? NSImage() }

		//nonPurchased course
		static var cart: NSImage { return NSImage(named: "cart") ?? NSImage() }

		//Preview
		static var playListItem: NSImage { return NSImage(named: "playListItem") ?? NSImage() }

		//Wifi Transfer
		static var server: NSImage { return NSImage(named: "server") ?? NSImage() }
		static var serverHighlighted: NSImage { return NSImage(named: "serverHighlighted") ?? NSImage() }
		static var uploads: NSImage { return NSImage(named: "uploads") ?? NSImage() }

	}
	
	struct color {
		static let primary: NSColor = NSColor(named: "primary") ?? .blue
		static let secondary: NSColor = NSColor(named: "secondary") ?? .red
		static let primaryTextLight: NSColor = NSColor(named: "textPrimaryLight") ?? .white
		static let primaryTextDark: NSColor = NSColor(named: "textPrimaryDark") ?? .black
		static let secondaryTextDark: NSColor = NSColor(named: "textSecondaryDark") ?? .lightGray
		static let subHeadTextDark: NSColor = NSColor(named: "textSubHead") ?? .lightGray
		static let separator: NSColor = NSColor(named: "separator") ?? .lightGray
		static let lightBackground: NSColor = NSColor(named: "lightBackground") ?? .lightGray
		static let success: NSColor = NSColor(named: "success") ?? .green
		static let textTertiaryDark: NSColor = NSColor(named: "textTertiaryDark") ?? .green
		static let darkBackground: NSColor = NSColor(named: "darkBackground") ?? .green
	}

	struct font {
		static func robotoRegular(size: CGFloat) -> NSFont {
			return NSFont(name: "Roboto-Regular", size: size)!
		}

		static func robotoBold(size: CGFloat) -> NSFont {
			return NSFont(name: "Roboto-Bold", size: size)!
		}

		static func robotoMedium(size: CGFloat) -> NSFont {
			return NSFont(name: "Roboto-Medium", size: size)!
		}
	}

	struct dimension {
		static var screenSize: CGSize {
			let isFullScreen = NSApplication.shared.windows.first?.styleMask.contains(.fullScreen) ?? false
			if let size = NSScreen.main?.visibleFrame.size, !isFullScreen{
				return CGSize(width: size.width, height: size.height-20)
			}else {
				return  CGSize(width: 970, height: 700)
			}
		}
		static var sideMenuSize: CGSize {
			return CGSize(width: 250, height: screenSize.height)
		}
		static var contentSize: CGSize {
			let size = screenSize
			return CGSize(width: size.width-sideMenuSize.width, height: size.height)
		}
	}

	struct apiError {
		static let sessionExpired = "You need to login"
	}
}
