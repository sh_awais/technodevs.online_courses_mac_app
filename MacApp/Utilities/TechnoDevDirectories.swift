//
//  TechnoDevDirectories.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation
import AppKit
import CCNNavigationController

class TechnoDevDirectories {
	fileprivate static let documentsDirectory: URL = {
		let urls = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)
		return urls[urls.endIndex - 1]
	}()

	fileprivate static let appDirectory: URL = {
		let bundleId = Bundle.main.bundleIdentifier ?? "com.technodev.app"
		let directory: URL = documentsDirectory.appendingPathComponent(bundleId)
		return directory
	}()

	static var userDirectory: URL {
		let directory: URL = appDirectory.appendingPathComponent("\(LoginSession.current.email)")
		return directory
	}
	
	static var uploadsDirectory: URL {
		let directory: URL = userDirectory.appendingPathComponent("Uploads/UserFiles")
		return directory
	}

	enum MediaDirectoryType: String {
		case video = "video"
		case audio = "audio"
		case ebook = "ebook"
	}
}

extension URL {

	func mediaDirectory(type: TechnoDevDirectories.MediaDirectoryType) -> URL {
		return self.appendingPathComponent(type.rawValue)
	}

	func mediaDirectory(fileExtension: FileExtensions) -> URL {
		let mediaType: TechnoDevDirectories.MediaDirectoryType
		switch fileExtension {
		case .mp3:
			mediaType = .audio
		case .mp4:
			mediaType = .video
		case .pdf,.epub:
			mediaType = .ebook
		case .unknown:
		return self
		}
		return mediaDirectory(type: mediaType)
	}

	func isDirectoryExists() -> Bool {
		var path = self.absoluteString
		if path.last == "/"{
			path = String(path.dropLast())
		}
		var isDir: ObjCBool = false
		if FileManager.default.fileExists(atPath: self.absoluteString, isDirectory: &isDir) {
			if isDir.boolValue {
				return true
			} else {
				return false
			}
		} else {
			return false
		}
	}

	var isFileExists: Bool {
		return FileManager.default.fileExists(atPath: self.path)
	}

	func createDirectory() -> URL? {
		let fileManager = FileManager.default
		if !fileManager.fileExists(atPath: self.path) {
			do {
				try fileManager.createDirectory(atPath: self.path, withIntermediateDirectories: true, attributes: nil)
				return self
			} catch {
				NSLog("Couldn't create document directory")
				return nil
			}
		}else {
			return self
		}
	}
}
