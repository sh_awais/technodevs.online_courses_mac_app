//
//  ImageHelper.swift
//  MacApp
//
//  Created by Mansoor Ali on 16/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import AppKit
import SDWebImage

class ImageHelper {
	static let shared = ImageHelper()

	func image(forCourse course: Course, size: Int, completion: @escaping (NSImage)->Void)  {

		let localPath = imagePath(forCourse: course, size: size)
		if let image = image(localUrl: localPath) {
			completion(image)
		}else if let url = course.thumbnailUrl(size: size) {
			_ = FileApi.provider.requestNormal(FileApi.download(url: url, fileDestinationUrl: localPath), callbackQueue: nil, progress: nil) { [weak self] (result) in
				guard let self = self else { return }
				switch result {
				case .success(_):
					completion(self.image(localUrl: localPath) ?? NSImage())
				case .failure(_):
					break
				}
			}
		}
	}

	func imagePath(forCourse course: Course, size: Int) -> URL {
		let fileName = "defaultx\(size)"
//		let fileExtension = (course.image_url as NSString).pathExtension

		let fileDirectory = TechnoDevDirectories.userDirectory
				.appendingPathComponent("\(course.category)")
				.appendingPathComponent("\(course.title)")
				.appendingPathComponent("\(fileName)")
				.appendingPathExtension("png")

		return fileDirectory
	}

	func image(localUrl url: URL) -> NSImage? {
		return NSImage(contentsOf: url)
	}
}
