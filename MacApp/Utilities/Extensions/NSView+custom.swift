//
//  NSView+custom.swift
//  MacApp
//
//  Created by Mansoor Ali on 12/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

extension NSView {

	func showTestBorder(color: NSColor = NSColor.red)  {
		self.wantsLayer = true
		self.layer?.borderColor = color.cgColor
		self.layer?.borderWidth = 1
	}
}
