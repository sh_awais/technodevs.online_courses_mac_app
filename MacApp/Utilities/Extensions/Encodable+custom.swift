//
//  Encodable+custom.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

extension Encodable {

	var jsonEncodedData: Data? {
		return try? JSONEncoder().encode(self)
	}

	var json: [String:Any] {
		guard let data =  jsonEncodedData else {
			return [:]
		}
		guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [])  else {
			return [:]
		}
		guard let dictionary =  jsonObject as? [String:Any] else {
			return [:]
		}
		return dictionary
	}
}
