//
//  NSButton.swift
//  MacApp
//
//  Created by Mansoor Ali on 07/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

extension NSButton {

	func titleWithColor(color: NSColor) -> NSAttributedString {
		let attributes = [
			NSAttributedString.Key.foregroundColor: color
		]
		return NSAttributedString(string: title, attributes: attributes)
	}

	func addBottomBorder(color: NSColor)  {
		self.wantsLayer = true
		let border = CALayer()
		border.backgroundColor = color.cgColor
		border.frame = CGRect(x: 0, y: self.frame.size.height-1, width: self.frame.size.width, height: 1)
		self.layer?.addSublayer(border)
	}
}
