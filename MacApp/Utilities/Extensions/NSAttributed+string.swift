//
//  NSAttributed+string.swift
//  MacApp
//
//  Created by Mansoor Ali on 09/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import AppKit

extension NSAttributedString {

	public static func stringFromComponents(firstComponent: String, firstFont: NSFont, firstTextColor: NSColor,
										 otherComponent: String, otherfont: NSFont, otherTextColor: NSColor) -> NSAttributedString {


		let firstAttrs = [NSAttributedString.Key.font: firstFont, NSAttributedString.Key.foregroundColor: firstTextColor]
		let firstAttributedString = NSAttributedString(string:"\(firstComponent) ", attributes: firstAttrs)

		let otherAttrs = [NSAttributedString.Key.font: otherfont, NSAttributedString.Key.foregroundColor: otherTextColor]
		let otherAttributedString = NSAttributedString(string:"\(otherComponent)", attributes: otherAttrs)

		let finalAttributedString: NSMutableAttributedString!
		finalAttributedString = NSMutableAttributedString(attributedString: firstAttributedString)
		finalAttributedString.append(otherAttributedString)

		return finalAttributedString
	}
}
