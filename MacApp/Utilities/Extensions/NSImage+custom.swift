//
//  NSImage.swift
//  MacApp
//
//  Created by Mansoor Ali on 06/03/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

extension NSImage {

	func tintedImage(tint: NSColor) -> NSImage {
		guard let tinted = self.copy() as? NSImage else { return self }
		tinted.lockFocus()
		tint.set()

		let imageRect = NSRect(origin: NSZeroPoint, size: self.size)
//		NSRectFillUsingOperation(imageRect, .sourceAtop)
		imageRect.fill(using: .sourceAtop)

		tinted.unlockFocus()
		return tinted
	}
    
}
