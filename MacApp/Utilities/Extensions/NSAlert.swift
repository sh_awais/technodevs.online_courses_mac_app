//
//  NSAlert.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

extension NSAlert {

	static func newAlert(message: String, info: String, style: NSAlert.Style) -> NSAlert {
		let alert: NSAlert = NSAlert()
		alert.messageText = message
		alert.informativeText = info
		alert.alertStyle = style
		return alert
	}

	static func showLogoutConfirmationAlert(completion: @escaping(Bool) -> Void) {
		let alert = NSAlert.newAlert(message: "Are you sure?", info: "", style: .critical)
		alert.addButton(withTitle: "Logout")
		alert.addButton(withTitle: "Cancel")
		let response = alert.runModal()
		if response == NSApplication.ModalResponse.alertFirstButtonReturn {
//			LoginSession.current.logout()
			completion(true)
		}else {
			completion(false)
		}
	}
}
