//
//  String+custom.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Foundation

extension String {
	public func isValid(forExp exp: String) -> Bool {
		guard let test = NSPredicate(format:"SELF MATCHES %@", exp) as NSPredicate? else { return false }
		return test.evaluate(with: self)
	}
}

