//
//  File.swift
//  MacApp
//
//  Created by Mansoor Ali on 15/02/2019.
//  Copyright © 2019 Mansoor Ali. All rights reserved.
//

import Cocoa

extension NSViewController {

	func expandToFulScreen() {
		let size = NSScreen.main?.frame.size ?? CGSize(width: 970, height: 700)
		self.view.frame.size = size
	}

	func setSize(greaterThanEqualTo size: CGSize)  {
		self.view.translatesAutoresizingMaskIntoConstraints = false
		self.view.widthAnchor.constraint(greaterThanOrEqualToConstant: size.width).isActive = true
		self.view.heightAnchor.constraint(greaterThanOrEqualToConstant: size.height).isActive = true
	}

	func showOnFullScreen() {
		let presOptions: NSApplication.PresentationOptions = [.fullScreen, .autoHideMenuBar]
		let optionsDictionary = [NSView.FullScreenModeOptionKey.fullScreenModeApplicationPresentationOptions: presOptions]
		view.enterFullScreenMode(NSScreen.main!, withOptions: optionsDictionary)
		view.wantsLayer = true
	}

}
